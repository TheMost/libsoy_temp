/*
 *  libsoy - soy.scenes.LandscapeQuad
 *  Copyright (C) 2006-2015 Copyleft Games Group
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, see http://www.gnu.org/licenses
 *
 */

[indent=4]
uses
    GLib
    GLib.Math
    GL
    Gee
    ode

class quadData : Object
    parent : weak quadData
    child : array of quadData
    
    quadSize : int
    divided : bool
    level : int

class soy.scenes.LandscapeQuad : soy.scenes.Scene
    _ebo : GLuint   // Element Buffer Object
    _ebo_lines : GLuint   // Element Buffer Object
    _vbo : GLuint   // Vertex Buffer Object
    _updated : bool // Buffers need updating
    _detail : int
    _vertArray : array of GLfloat
    _numVert : int
    _numVertLines : int
    _faceArray : array of GLushort
    _faceArrayLines : array of GLushort
    _heightfieldData : ode.geoms.HeightfieldData
    _geomID : ode.geoms.Heightfield
    _landscapeBodies : new dict of string, soy.bodies.LandscapeBody
    _resolutionChanged : bool
    _firstRenderPass : bool
    prev : float
    level : int

    construct (heightmap : soy.textures.Texture, mat : soy.materials.Material,
               detail : int = 1,
               size : soy.atoms.Size?,
               position : soy.atoms.Position?)
        assert heightmap.channels is 1

        self._map = heightmap
        self._material = mat
        self._detail = detail < 1 ? 1 : detail

        if size is null
            self._size = new soy.atoms.Size (1024, 16, 1024)
        else
            self._size = size

        if position is null
            self._pos = new soy.atoms.Position ()
        else
            self._pos = position

        level  = 0
        _resolutionChanged = true
        // Setup for first render pass
        self._updated = true
        _firstRenderPass = true

    ////////////////////////////////////////////////////////////////////////
    // Properties

    _material : soy.materials.Material
    prop material : soy.materials.Material
        get
            return _material
        set
            self._material = value

    _pos : soy.atoms.Position
    prop new position : soy.atoms.Position
        get
            return _pos
        set
            self._pos = value

    _size : soy.atoms.Size
    prop size : soy.atoms.Size
        get
            return _size
        set
            self._size = value

    // patch size exponend
   _lod_distance : array of int
    prop lod_distance : array of int
        get
            return _lod_distance
        set
            self._lod_distance = value


    _map : soy.textures.Texture
    prop heightmap : soy.textures.Texture
        get
            return _map
        set
            self._map = value
            self._updated = true


    ////////////////////////////////////////////////////////////////////////
    // Methods
    // def _update ()

    // def _render ()

    def _update_landscape ()
        //
        // Each texel represents a vertex:
        // (3x3 texture, viewed from above)
        //
        // 0----1----2
        // |    |    |
        // |    |    |
        // 3----4----5
        // |    |    |
        // |    |    |
        // 6----7----8
        //

        // on the first pass
        if _ebo == 0
            buffers : array of GLuint = {0,0,0}
            glGenBuffers(buffers)
            _ebo = buffers[0]
            _ebo_lines = buffers[1]
            _vbo = buffers[2]

//        deltaCols, deltaRows : float *
//        maxDelta, minDelta : float

        v1 : array of double = new array of double[3]
        v2 : array of double = new array of double[3]
        normal : array of double = new array of double[3]
        length : double

        self._heightfieldData = new ode.geoms.HeightfieldData()

        //self._numVert = ((int)_map.size.width-1) * ((int)_map.size.height-1) * 6
        //self._numVertLines = ((int)_map.size.width-1) * ((int)_map.size.height-1) * 6 + ((int)_map.size.width + (int)_map.size.height-2) * 2
        self._numVert = 24 * ((int)_map.size.width * (int)_map.size.width)/4
        self._numVertLines =  40 * ((int)_map.size.width * (int)_map.size.width)/4

        self._vertArray = new array of GLfloat[((int)_map.size.width) * ((int)_map.size.height) * 11]
        self._faceArray = new array of GLushort[self._numVert]
        self._faceArrayLines = new array of GLushort[self._numVertLines]

        self._heightfieldData.BuildByte(_map.texels,
                                        0,                          // copy?
                                        _size.width,                // width
                                        _size.depth,                // depth
                                        (int) _map.size.width,      // dataX
                                        (int) _map.size.height,     // dataY
                                        (Real) (1.0f / 255.0f *
                                                _size.height),      // scale
                                        0,                          // offset
                                        (Real) 4.0f,                // thick
                                        0                           // wrapped
                                       )

        self._geomID = new ode.geoms.Heightfield(space, _heightfieldData, 1)
        self._geomID.SetPosition(_pos.x, _pos.y, _pos.z)

        // Alloc _delta arrays
        //
        //   These are arrays for column and row edge deltas:
        //     . . . . . . . .    ._._._._._._._.
        //     | | | | | | | | < delta
        //     . . . . . . . .    ._._._._._._._.
        //     | | | | | | | |         ^delta
        //     . . . . . . . .    ._._._._._._._.
        //        deltaCols          deltaRows
        //
        //   They're used in determining edge collapses in LOD generation below.
//        deltaCols = new array of float[((int)_map.size.width) * ((int)_map.size.height)]
//        deltaRows = new array of float[((int)_map.size.width) * ((int)_map.size.height)]      

        // Calculate positions and texcoords first
        for var i = 0 to (_map.size.height - 1)
            for var j = 0 to (_map.size.width - 1)
                var l = (int)_map.size.width * i + j
                _vertArray[l * 11]     = (j / (_map.size.width - 1.0f) - 0.5f) * _size.width
                _vertArray[l * 11 + 1] = (_map.texels[l] / 255.0f) * _size.height
                _vertArray[l * 11 + 2] = (i / (_map.size.height - 1.0f) - 0.5f)  * _size.depth
                // all below need to be calculated CORRECTLY!
                _vertArray[l * 11 + 6] = j                         // * texture scale
                _vertArray[l * 11 + 7] = i                         // * texture scale
                // _vertArray[l].texcoord.z = _map.texels[l] / 255.0f

        // Normals and tangents calculated second because they depend on position
        for var i = 0 to (_map.size.height - 1)
            for var j = 0 to (_map.size.width - 1)
                var offset = (int)_map.size.width * i + j

                //   u     c = current vert
                //   |     u/d/l/r = up/down/left/right vert
                // l-c-r
                //   |     vector1 = lr
                //   d     vector2 = ud
                var u = offset - (int)_map.size.width
                var d = offset + (int)_map.size.width
                var l = offset - 1
                var r = offset + 1
                var c = offset

                // check if we are at the boundaries
                if i is 0
                    u = c
                if i is (_map.size.height - 1)
                    d = c
                if j is 0
                    l = c
                if j is (_map.size.height - 1)
                    r = c

                v1[0] = _vertArray[r * 11 + 0] - _vertArray[l * 11 + 0]
                v1[1] = _vertArray[r * 11 + 1] - _vertArray[l * 11 + 1]
                v1[2] = _vertArray[r * 11 + 2] - _vertArray[l * 11 + 2]

                v2[0] = _vertArray[d * 11 + 0] - _vertArray[u * 11 + 0]
                v2[1] = _vertArray[d * 11 + 1] - _vertArray[u * 11 + 1]
                v2[2] = _vertArray[d * 11 + 2] - _vertArray[u * 11 + 2]

                // While we are in this loop, calculate the delta map for LOD
                // deltaRows[offset] = _vertArray[offset * 11 + 1] - _vertArray[(offset+1) * 11 + 1]

                // perform cross products on the two vectors
                normal[0] = v2[1]*v1[2]-v2[2]*v1[1] // Calculate the x component of the normal
                normal[1] = v2[2]*v1[0]-v2[0]*v1[2] // Calculate the y component of the normal
                normal[2] = v2[0]*v1[1]-v2[1]*v1[0] // Calculate the z component of the normal

                length = sqrt(normal[0] * normal[0] +
                              normal[1] * normal[1] +
                              normal[2] * normal[2])

                normal[0] /= length
                normal[1] /= length
                normal[2] /= length

                _vertArray[offset * 11 + 3] = (float) normal[0]
                _vertArray[offset * 11 + 4] = (float) normal[1]
                _vertArray[offset * 11 + 5] = (float) normal[2]

                // set tangent as vector1
                length = sqrt(v1[0] * v1[0] +
                              v1[1] * v1[1] +
                              v1[2] * v1[2])

                v1[0] /= length
                v1[1] /= length
                v1[2] /= length

                self._vertArray[offset * 11 + 8] = (float) v1[0]
                self._vertArray[offset * 11 + 9] = (float) v1[1]
                self._vertArray[offset * 11 + 10] = (float) v1[2]

        glBindBuffer(GL_ARRAY_BUFFER, _vbo)
        glBufferData(GL_ARRAY_BUFFER, (GLsizei) (((int)_map.size.width) *
                     ((int)_map.size.height) * 11 * sizeof(GLfloat)),
                     _vertArray,
                     GL_STATIC_DRAW)

        // Set a starting value for _maxDelta and _minDelta so it isn't blank
//        maxDelta = deltaRows[0]
//        minDelta = deltaRows[0]

        // Loop through _deltaRows to figure out what can be merged
//        for i = 0 to (((int)_map.size.width + 1) * ((int)_map.size.height + 1) - 1)
//            if deltaRows[i] > maxDelta
//                maxDelta = deltaRows[i]
//            if deltaRows[i] < minDelta
//                minDelta = deltaRows[i]

        self._updated = false

    // def _addQuadNode(parent : quadData) : quadData
    //     node : quadData = new quadData
    //     node.quadSize = parent.quadSize/2
    //     node.parent = parent
    //     node.child = new array of quadData[4]   
    //     node.divided = false
    //     node.level = parent.level + 1
    //     return node

    def _drawQuad (i : int , j : int, l : int, m : int, qsize : int)
        _render_square(i, j, l, m, qsize/2, true)
        l += 6
        m +=10
        _render_square(i+qsize/2, j, l, m, qsize/2, false)
        l += 6
        m +=10
        _render_square(i, j+qsize/2, l, m, qsize/2, false)
        l += 6
        m +=10
        _render_square(i+qsize/2, j+qsize/2, l, m, qsize/2, true)

    def _update_quads (camera : soy.bodies.Camera)

        var l = 0
        var m = 0
        var steps = (int)(_map.size.width)-1
        var count = -1
        while steps >= 2
            steps = steps/2
            count++

        var dist = Math.sqrt(position.distance_squared(camera.position))
        size : int = (int)((_map.size.width-1)/(Math.pow(2, level)))

        if(dist/size >= 20 && level >= 0)
            level--
            _resolutionChanged = true
        else if(size/dist > 0.3 && level < count)
            level++
            _resolutionChanged = true
        
        if _resolutionChanged || _firstRenderPass
            print "Hello"
            size  = (int)((_map.size.width-1)/(Math.pow(2, level)))
            for var i = 0 to (Math.pow(2,level)-1)
                for var j = 0 to (Math.pow(2,level)-1)
                    _drawQuad(i*size, j*size, l, m, size)
                    l += 24
                    m += 40
            _resolutionChanged = false
            _firstRenderPass = false


    def _render_square (i : int , j : int, l : int, m : int, qsize : int, diagonalFlag : bool)

        // CCW winding order:
        //   a-c
        //   |/|   abc,bdc
        //   b-d

        //   a-c
        //   |\|   abd,adc
        //   b-d
        var a = (GLushort) (j + ( i * (_map.size.width)))
        var b = (GLushort) (j + ((i + qsize) * (_map.size.width)))
        var c = (GLushort) (a + qsize)
        var d = (GLushort) (b + qsize)
        _faceArray[l]   = a
        _faceArray[l+1] = b
        if diagonalFlag == false
            _faceArray[l+2] = c
            _faceArray[l+3] = b
        else
            _faceArray[l+2] = d
            _faceArray[l+3] = a

        _faceArray[l+4] = d
        _faceArray[l+5] = c

        // ab ac bc/ad
        _faceArrayLines[m]   = a
        _faceArrayLines[m+1] = b
        _faceArrayLines[m+2] = a
        _faceArrayLines[m+3] = c
        if diagonalFlag == false
            _faceArrayLines[m+4] = b
            _faceArrayLines[m+5] = c
        else
            _faceArrayLines[m+4] = a
            _faceArrayLines[m+5] = d

         _faceArrayLines[m+6] = c 
         _faceArrayLines[m+7] = d

         _faceArrayLines[m+8] = b 
         _faceArrayLines[m+9] = d

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ebo)
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, (GLsizei) (_numVert * sizeof(GLushort)),
                     _faceArray,
                     GL_STATIC_DRAW)

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ebo_lines)
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, (GLsizei) (_numVertLines * sizeof(GLushort)),
                     _faceArrayLines,
                     GL_STATIC_DRAW)

    _prevCameraPosition : soy.atoms.Vector? = null

    def override render_extra (view : array of GLfloat, projection : array of
                               GLfloat, camera : soy.bodies.Camera, lights :
                               array of soy.bodies.Light)
        // Lock so body cant be changed during render

        // modelview matrix
        model_view : array of GLfloat[16] = view

        if(prev - position.distance_squared(camera.position) != 0)
            self._updated = true

        if _updated
            _update_landscape()
            _update_quads(camera)

        else
            glBindBuffer(GL_ARRAY_BUFFER, _vbo)

        prev = position.distance_squared(camera.position)

        if not(self._material.draw_mode is GL_LINES or self._material.draw_mode is GL_TRIANGLES)
            print "Landscape material draw mode might not be supported"
        
        var numVert = self._numVert
        if self._material.draw_mode is GL_LINES
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ebo_lines)
            numVert = self._numVertLines
        else
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ebo)

        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, (GLsizei)
                              (sizeof(GLfloat) * 11), null)
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, (GLsizei)
                              (sizeof(GLfloat) * 11), (GLvoid*)
                              (sizeof(GLfloat) * 3))
        glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, (GLsizei)
                              (sizeof(GLfloat) * 11), (GLvoid*)
                              (sizeof(GLfloat) * 6))
        glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, (GLsizei)
                              (sizeof(GLfloat) * 11), (GLvoid*)
                              (sizeof(GLfloat) * 8))

        glEnableVertexAttribArray(0)
        glEnableVertexAttribArray(1)
        glEnableVertexAttribArray(2)
        glEnableVertexAttribArray(3)

        i : int = 0
        while self._material.enable(i, model_view, view, projection, lights,
                                    ambient.get4f(), fog.get4f())
            glDrawElements(self._material.draw_mode, numVert, GL_UNSIGNED_SHORT,
                           (GLvoid*) 0)
            i++

        glDisableVertexAttribArray(0)
        glDisableVertexAttribArray(1)
        glDisableVertexAttribArray(2)
        glDisableVertexAttribArray(3)

        self._material.disable()

        // Release lock
