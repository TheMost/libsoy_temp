/*
 *  libsoy - soy.scenes.Dungeon
 *  Copyright (C) 2006-2015 Copyleft Games Group
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, see http://www.gnu.org/licenses
 *
 */

[indent=4]
uses
    GLib
    GLib.Math
    GL
    Gee
    ode


class soy.scenes.Dungeon : soy.scenes.Scene

    init
        _rooms = new Scenecontainer()

    construct ()
        print "dungeon created"


    ////////////////////////////////////////////////////////////////////////
    // Methods

    def connect_rooms(room1 : soy.scenes.Room, room2 : soy.scenes.Room,
                        doorX : float, doorY : float, doorWidth : float,
                        doorHeight : float)
        room1pos : soy.atoms.Position = room1.position
        room2pos : soy.atoms.Position = room2.position
        room1size : soy.atoms.Size = room1.size
        room2size : soy.atoms.Size = room2.size

        room1wallid : int = 0
        room2wallid : int = 0
        room1x1 : float = 0.0f
        room1x2 : float = 0.0f
        room1y1 : float = 0.0f
        room1y2 : float = 0.0f
        room2x1 : float = 0.0f
        room2x2 : float = 0.0f
        room2y1 : float = 0.0f
        room2y2 : float = 0.0f
        portal1pos : soy.atoms.Position = new soy.atoms.Position()
        portal2pos : soy.atoms.Position = new soy.atoms.Position()
        portal1size : soy.atoms.Size = new soy.atoms.Size()
        portal2size : soy.atoms.Size = new soy.atoms.Size()

        //if room1's front wall is common
        if room1pos.z + room1size.depth/2 is room2pos.z - room2size.depth/2
            room1wallid = 1
            room2wallid = 3

            //calculate common wall portion
            leftextreme : float = (float)GLib.Math.fmax(
                                            room1pos.x - room1size.width/2,
                                            room2pos.x - room2size.width/2)
            rightextreme : float = (float)GLib.Math.fmin(
                                            room1pos.x + room1size.width/2,
                                            room2pos.x + room2size.width/2)
            lowextreme : float = (float)GLib.Math.fmax(
                                            room1pos.y - room1size.height/2,
                                            room2pos.y - room2size.height/2)
            highextreme : float = (float)GLib.Math.fmin(
                                            room1pos.y + room1size.height/2,
                                            room2pos.y + room2size.height/2)

            //if door size is greater than common portion of the wall, 
            //make door fit the common portion of the wall
            if doorWidth > rightextreme - leftextreme
                doorWidth = rightextreme - leftextreme
            if doorHeight > highextreme - lowextreme
                doorHeight = highextreme - lowextreme

            //actual door coordinates
            x1 : float = (leftextreme + rightextreme)/2 + doorX - doorWidth/2
            x2 : float = (leftextreme + rightextreme)/2 + doorX + doorWidth/2
            y1 : float = lowextreme + doorY
            y2 : float = lowextreme + doorY + doorHeight

            //calculate parameters for Room.add_door()
            room1x1 = x1 - (room1pos.x - room1size.width/2)
            room1x2 = x2 - (room1pos.x - room1size.width/2)
            room1y1 = y1 - (room1pos.y - room1size.height/2)
            room1y2 = y2 - (room1pos.y - room1size.height/2)
            room2x1 = (room2pos.x + room2size.width/2) -x1
            room2x2 = (room2pos.x + room2size.width/2) -x2
            room2y1 = y1 - (room2pos.y - room2size.height/2)
            room2y2 = y2 - (room2pos.y - room2size.height/2)

            //calculate parameters for Portal constructor
            portal1pos.x = (x1 + x2)/2 - room1pos.x
            portal1pos.y = (y1 + y2)/2 - room1pos.y
            portal1pos.z = room1size.depth/2
            portal2pos.x = (x1 + x2)/2 - room2pos.x
            portal2pos.y = (y1 + y2)/2 - room2pos.y
            portal2pos.z = -room2size.depth/2

            portal1size.width = doorWidth
            portal1size.height = doorHeight
            portal1size.depth = room1.wall_width
            portal2size.width = doorWidth
            portal2size.height = doorHeight
            portal2size.depth = room2.wall_width

        //if room1's back wall is common
        else if room2pos.z + room2size.depth/2 is (
                                            room1pos.z - room1size.depth/2)
            room2wallid = 1
            room1wallid = 3

            //calculate common wall portion
            leftextreme : float = (float)GLib.Math.fmax(
                                            room1pos.x - room1size.width/2,
                                            room2pos.x - room2size.width/2)
            rightextreme : float = (float)GLib.Math.fmin(
                                            room1pos.x + room1size.width/2,
                                            room2pos.x + room2size.width/2)
            lowextreme : float = (float)GLib.Math.fmax(
                                            room1pos.y - room1size.height/2,
                                            room2pos.y - room2size.height/2)
            highextreme : float = (float)GLib.Math.fmin(
                                            room1pos.y + room1size.height/2,
                                            room2pos.y + room2size.height/2)

            //if door size is greater than common portion of the wall, 
            //make door fit the common portion of the wall
            if doorWidth > rightextreme - leftextreme
                doorWidth = rightextreme - leftextreme
            if doorHeight > highextreme - lowextreme
                doorHeight = highextreme - lowextreme

            //actual door coordinates
            x1 : float = (leftextreme + rightextreme)/2 + doorX - doorWidth/2
            x2 : float = (leftextreme + rightextreme)/2 + doorX + doorWidth/2
            y1 : float = lowextreme + doorY
            y2 : float = lowextreme + doorY + doorHeight

            //calculate parameters for Room.add_door()
            room2x1 = x1 - (room2pos.x - room2size.width/2)
            room2x2 = x2 - (room2pos.x - room2size.width/2)
            room2y1 = y1 - (room2pos.y - room2size.height/2)
            room2y2 = y2 - (room2pos.y - room2size.height/2)
            room1x1 = (room1pos.x + room1size.width/2) -x1
            room1x2 = (room1pos.x + room1size.width/2) -x2
            room1y1 = y1 - (room1pos.y - room1size.height/2)
            room1y2 = y2 - (room1pos.y - room1size.height/2)

            //calculate parameters for Portal constructor
            portal1pos.x = (x1 + x2)/2 - room1pos.x
            portal1pos.y = (y1 + y2)/2 - room1pos.y
            portal1pos.z = -room2size.depth/2
            portal2pos.x = (x1 + x2)/2 - room2pos.x
            portal2pos.y = (y1 + y2)/2 - room2pos.y
            portal2pos.z = room1size.depth/2

            portal1size.width = doorWidth
            portal1size.height = doorHeight
            portal1size.depth = room1.wall_width
            portal2size.width = doorWidth
            portal2size.height = doorHeight
            portal2size.depth = room2.wall_width


        //if room1's right wall is common
        else if room1pos.x + room1size.width/2 is (
                                            room2pos.x - room2size.width/2)
            room1wallid = 2
            room2wallid = 4

            //calculate common wall portion
            farextreme : float = (float)GLib.Math.fmax(
                                            room1pos.z - room1size.depth/2,
                                            room2pos.z - room2size.depth/2)
            nearextreme : float = (float)GLib.Math.fmin(
                                            room1pos.z + room1size.depth/2,
                                            room2pos.z + room2size.depth/2)
            lowextreme : float = (float)GLib.Math.fmax(
                                            room1pos.y - room1size.height/2,
                                            room2pos.y - room2size.height/2)
            highextreme : float = (float)GLib.Math.fmin(
                                            room1pos.y + room1size.height/2,
                                            room2pos.y + room2size.height/2)

            //if door size is greater than common portion of the wall, 
            //make door fit the common portion of the wall
            if doorWidth > nearextreme - farextreme
                doorWidth = nearextreme - farextreme
            if doorHeight > highextreme - lowextreme
                doorHeight = highextreme - lowextreme

            //actual door coordinates
            x1 : float = (farextreme + nearextreme)/2 + doorX - doorWidth/2
            x2 : float = (farextreme + nearextreme)/2 + doorX + doorWidth/2
            y1 : float = lowextreme + doorY
            y2 : float = lowextreme + doorY + doorHeight

            //calculate parameters for Room.add_door()
            room1x1 = (room1pos.z + room1size.depth/2) -x1
            room1x2 = (room1pos.z + room1size.depth/2) - x2
            room1y1 = y1 - (room1pos.y - room1size.height/2)
            room1y2 = y2 - (room1pos.y - room1size.height/2)
            room2x1 = x1 - (room2pos.z - room2size.depth/2)
            room2x2 = x2 - (room2pos.z - room2size.depth/2)
            room2y1 = y1 - (room2pos.y - room2size.height/2)
            room2y2 = y2 - (room2pos.y - room2size.height/2)

            //calculate parameters for Portal constructor
            portal1pos.x = room1size.width/2
            portal1pos.y = (y1 + y2)/2 - room1pos.y
            portal1pos.z = (x1 + x2)/2 - room1pos.z
            portal2pos.x = -room2size.width/2
            portal2pos.y = (y1 + y2)/2 - room2pos.y
            portal2pos.z = (x1 + x2)/2 - room2pos.z

            portal1size.width = room1.wall_width
            portal1size.height = doorHeight
            portal1size.depth = doorWidth
            portal2size.width = room2.wall_width
            portal2size.height = doorHeight
            portal2size.depth = doorWidth

        //if room1's left wall is common
        else if room2pos.x + room2size.width/2 is (
                                            room1pos.x - room1size.width/2)
            room2wallid = 2
            room1wallid = 4

            //calculate common wall portion
            farextreme : float = (float)GLib.Math.fmax(
                                            room1pos.z - room1size.depth/2,
                                            room2pos.z - room2size.depth/2)
            nearextreme : float = (float)GLib.Math.fmin(
                                            room1pos.z + room1size.depth/2,
                                            room2pos.z + room2size.depth/2)
            lowextreme : float = (float)GLib.Math.fmax(
                                            room1pos.y - room1size.height/2,
                                            room2pos.y - room2size.height/2)
            highextreme : float = (float)GLib.Math.fmin(
                                            room1pos.y + room1size.height/2,
                                            room2pos.y + room2size.height/2)

            //if door size is greater than common portion of the wall, 
            //make door fit the common portion of the wall
            if doorWidth > nearextreme - farextreme
                doorWidth = nearextreme - farextreme
            if doorHeight > highextreme - lowextreme
                doorHeight = highextreme - lowextreme

            //actual door coordinates
            x1 : float = (farextreme + nearextreme)/2 + doorX - doorWidth/2
            x2 : float = (farextreme + nearextreme)/2 + doorX + doorWidth/2
            y1 : float = lowextreme + doorY
            y2 : float = lowextreme + doorY + doorHeight

            //calculate parameters for Room.add_door()
            room2x1 = (room2pos.z + room2size.depth/2) -x1
            room2x2 = (room2pos.z + room2size.depth/2) - x2
            room2y1 = y1 - (room2pos.y - room2size.height/2)
            room2y2 = y2 - (room2pos.y - room2size.height/2)
            room1x1 = x1 - (room1pos.z - room1size.depth/2)
            room1x2 = x2 - (room1pos.z - room1size.depth/2)
            room1y1 = y1 - (room1pos.y - room1size.height/2)
            room1y2 = y2 - (room1pos.y - room1size.height/2)

            //calculate parameters for Portal constructor
            portal1pos.x = -room1size.width/2
            portal1pos.y = (y1 + y2)/2 - room1pos.y
            portal1pos.z = (x1 + x2)/2 - room1pos.z
            portal2pos.x = room2size.width/2
            portal2pos.y = (y1 + y2)/2 - room2pos.y
            portal2pos.z = (x1 + x2)/2 - room2pos.z

            portal1size.width = room1.wall_width
            portal1size.height = doorHeight
            portal1size.depth = doorWidth
            portal2size.width = room2.wall_width
            portal2size.height = doorHeight
            portal2size.depth = doorWidth

        //if room1's floor is common
        else if room1pos.y - room1size.height/2 is (
                                            room2pos.y + room2size.height/2)
            room1wallid = 5
            room2wallid = 6

            //calculate common wall portion
            leftextreme : float = (float)GLib.Math.fmax(
                                            room1pos.x - room1size.width/2,
                                            room2pos.x - room2size.width/2)
            rightextreme : float = (float)GLib.Math.fmin(
                                            room1pos.x + room1size.width/2,
                                            room2pos.x + room2size.width/2)
            farextreme : float = (float)GLib.Math.fmax(
                                            room1pos.z - room1size.depth/2,
                                            room2pos.z - room2size.depth/2)
            nearextreme : float = (float)GLib.Math.fmin(
                                            room1pos.z + room1size.depth/2,
                                            room2pos.z + room2size.depth/2)

            //if door size is greater than common portion of the wall, 
            //make door fit the common portion of the wall
            if doorWidth > rightextreme - leftextreme
                doorWidth = rightextreme - leftextreme
            if doorHeight > nearextreme - farextreme
                doorHeight = nearextreme - farextreme

            //actual door coordinates
            x1 : float = (leftextreme + rightextreme)/2 + doorX - doorWidth/2
            x2 : float = (leftextreme + rightextreme)/2 + doorX + doorWidth/2
            y1 : float = nearextreme - doorY
            y2 : float = nearextreme - doorY - doorHeight

            //calculate parameters for Room.add_door()
            room1x1 = x1 - (room1pos.x - room1size.width/2)
            room1x2 = x2 - (room1pos.x - room1size.width/2)
            room1y1 = y1 - (room1pos.z - room1size.depth/2)
            room1y2 = y2 - (room1pos.z - room1size.depth/2)
            room2x1 = x1 - (room2pos.x - room2size.width/2)
            room2x2 = x2 - (room2pos.x - room2size.width/2)
            room2y1 = (room2pos.z + room2size.depth/2) - y1
            room2y2 = (room2pos.z + room2size.depth/2) - y2

            //calculate parameters for Portal constructor
            portal1pos.x = (x1 + x2)/2 - room1pos.x
            portal1pos.y = -room1size.height/2
            portal1pos.z = (y1 + y2)/2 - room1pos.z
            portal2pos.x = (x1 + x2)/2 - room2pos.x
            portal2pos.y = room2size.height/2
            portal2pos.z = (y1 + y2)/2 - room2pos.z

            portal1size.width = doorWidth
            portal1size.height = room1.wall_width
            portal1size.depth = doorHeight
            portal2size.width = doorWidth
            portal2size.height = room2.wall_width
            portal2size.depth = doorHeight

        //if room1's ceiling is common
        else if room2pos.y - room2size.height/2 is (
                                            room1pos.y + room1size.height/2)
            room2wallid = 5
            room1wallid = 6

            //calculate common wall portion
            leftextreme : float = (float)GLib.Math.fmax(
                                            room1pos.x - room1size.width/2,
                                            room2pos.x - room2size.width/2)
            rightextreme : float = (float)GLib.Math.fmin(
                                            room1pos.x + room1size.width/2,
                                            room2pos.x + room2size.width/2)
            farextreme : float = (float)GLib.Math.fmax(
                                            room1pos.z - room1size.depth/2,
                                            room2pos.z - room2size.depth/2)
            nearextreme : float = (float)GLib.Math.fmin(
                                            room1pos.z + room1size.depth/2,
                                            room2pos.z + room2size.depth/2)

            //if door size is greater than common portion of the wall, 
            //make door fit the common portion of the wall
            if doorWidth > rightextreme - leftextreme
                doorWidth = rightextreme - leftextreme
            if doorHeight > nearextreme - farextreme
                doorHeight = nearextreme - farextreme

            //actual door coordinates
            x1 : float = (leftextreme + rightextreme)/2 + doorX - doorWidth/2
            x2 : float = (leftextreme + rightextreme)/2 + doorX + doorWidth/2
            y1 : float = nearextreme - doorY
            y2 : float = nearextreme - doorY - doorHeight

            //calculate parameters for Room.add_door()
            room2x1 = x1 - (room2pos.x - room2size.width/2)
            room2x2 = x2 - (room2pos.x - room2size.width/2)
            room2y1 = y1 - (room2pos.z - room2size.depth/2)
            room2y2 = y2 - (room2pos.z - room2size.depth/2)
            room1x1 = x1 - (room1pos.x - room1size.width/2)
            room1x2 = x2 - (room1pos.x - room1size.width/2)
            room1y1 = (room1pos.z + room1size.depth/2) - y1
            room1y2 = (room1pos.z + room1size.depth/2) - y2

            //calculate parameters for Portal constructor
            portal1pos.x = (x1 + x2)/2 - room1pos.x
            portal1pos.y = room1size.height/2
            portal1pos.z = (y1 + y2)/2 - room1pos.z
            portal2pos.x = (x1 + x2)/2 - room2pos.x
            portal2pos.y = -room2size.height/2
            portal2pos.z = (y1 + y2)/2 - room2pos.z

            portal1size.width = doorWidth
            portal1size.height = room1.wall_width
            portal1size.depth = doorHeight
            portal2size.width = doorWidth
            portal2size.height = room2.wall_width
            portal2size.depth = doorHeight


        else
            print "no common walls. no portal added"
            return


        room1.add_door(room1wallid, room1x1, room1y1, room1x2, room1y2)
        room2.add_door(room2wallid, room2x1, room2y1, room2x2, room2y2)
        room1portal : soy.bodies.Portal = new soy.bodies.Portal(portal1pos, 
                                                                    portal1size)
        room2portal : soy.bodies.Portal = new soy.bodies.Portal(portal2pos, 
                                                                    portal2size)
        //use geom id of portal when adding to room as key for uniqueness
        room1.set("portal"+((int)(room1portal.geom)).to_string(), room1portal)
        room2.set("portal"+((int)(room2portal.geom)).to_string(), room2portal)
        //set portal's target
        room1portal.target = room2portal
        room2portal.target = room1portal


    _rooms : soy.scenes.Scenecontainer
    prop rooms : soy.scenes.Scenecontainer
        get
            return self._rooms
        set 
            _rooms = value
