/*
 *  libsoy - soy.scenes.Scenecontainer
 *  Copyright (C) 2006-2015 Copyleft Games Group
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, see http://www.gnu.org/licenses
 *
 */

[indent=4]
uses
    GLib
    GLib.Math
    GL
    Gee
    ode

class soy.scenes.Scenecontainer : Object

    scenes : dict of string, soy.scenes.Scene
    mutex : Mutex

    construct()
        scenes = new dict of string, soy.scenes.Scene

    init
        mutex = Mutex()

    // Mapping
    def new get (key : string) : soy.scenes.Scene?
        res : soy.scenes.Scene? = null
        mutex.lock()
        if key in scenes.keys
            res = scenes[key]
        mutex.unlock()
        return res


    def has_key (key : string) : bool
        res : bool
        mutex.lock()
        res = scenes.has_key(key)
        mutex.unlock()
        return res


    def new set (key : string, value : soy.scenes.Scene?)
        mutex.lock()
        _stepLock.writer_lock()
        scenes[key] = value
        _stepLock.writer_unlock()
        mutex.unlock()


    prop length : ulong
        get
            res : ulong = 0
            mutex.lock()
            res = scenes.size
            mutex.unlock()
            return res

