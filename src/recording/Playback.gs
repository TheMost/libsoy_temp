/*
 *  libsoy - soy.recording.Playback.gs
 *  Copyright (C) 2006-2015 Copyleft Games Group
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, see http://www.gnu.org/licenses
 *
 */

[indent=4]
uses GL

class soy.recording.Playback : Object

    commandBufferSize : static int = 10000
    instructionBuff : static private list of EncodedCmd
    instructionIdx : static private int
    playbackStarted : static private bool = false
    playbackFinished : static bool = false
    playbackFile : static FileStream
    playbackFileName : private static string
    numberOfFramesPlayed : private static long = 0

    def static private loadInstructions()  
        if playbackFile == null
             playbackFile = FileStream.open(playbackFileName,"r")
        var ret = new list of EncodedCmd
        i : int
        for i = 0 to (commandBufferSize-1)
            var dontcare = new array of char[100000]
            var str = (!)playbackFile.gets(dontcare)
            if str == null
                break
            var split = str.split(" ")
            var type = (GlFunc)int.parse(split[0])
            var parameters = new array of string[split.length-1]
            x : int
            for x = 0 to (parameters.length-1)
                if x == parameters.length - 1
                    parameters[x] = split[x+1].substring(0,split[x+1].length-2)
                else
                    parameters[x] = split[x+1]
            var cmd = new EncodedCmd(type,parameters)
            ret.add(cmd)

        instructionIdx = 0
        instructionBuff = ret
        return

    def static private executeInstruction(cmd : EncodedCmd)
        case cmd.command
            when GlFunc.glActiveTexture
                PlaybackFuncs.playbackGlActiveTexture(cmd)
                return
            when GlFunc.glAttachShader
                PlaybackFuncs.playbackGlAttachShader(cmd)
                return
            when GlFunc.glBindAttribLocation
                PlaybackFuncs.playbackGlBindAttribLocation(cmd)
                return
            when GlFunc.glBindBuffer
                PlaybackFuncs.playbackGlBindBuffer(cmd)
                return
            when GlFunc.glBindFramebuffer
                PlaybackFuncs.playbackGlBindFramebuffer(cmd)
                return
            when GlFunc.glBindRenderbuffer
                PlaybackFuncs.playbackGlBindRenderbuffer(cmd)
                return
            when GlFunc.glBindTexture
                PlaybackFuncs.playbackGlBindTexture(cmd)
                return
            when GlFunc.glBlendColor
                PlaybackFuncs.playbackGlBlendColor(cmd)
                return
            when GlFunc.glBlendEquation
                PlaybackFuncs.playbackGlBlendEquation(cmd)
                return
            when GlFunc.glBlendEquationSeparate
                PlaybackFuncs.playbackGlBlendEquationSeparate(cmd)
                return
            when GlFunc.glBlendFunc
                PlaybackFuncs.playbackGlBlendFunc(cmd)
                return
            when GlFunc.glBlendFuncSeparate
                PlaybackFuncs.playbackGlBlendFuncSeparate(cmd)
                return
            when GlFunc.glBufferData
                PlaybackFuncs.playbackGlBufferData(cmd)
                return
            when GlFunc.glBufferSubData
                PlaybackFuncs.playbackGlBufferSubData(cmd)
                return
            when GlFunc.glCheckFramebufferStatus
                PlaybackFuncs.playbackGlCheckFramebufferStatus(cmd)
                return
            when GlFunc.glClear
                PlaybackFuncs.playbackGlClear(cmd)
                return
            when GlFunc.glClearColor
                PlaybackFuncs.playbackGlClearColor(cmd)
                return
            when GlFunc.glClearDepthf
                PlaybackFuncs.playbackGlClearDepthf(cmd)
                return
            when GlFunc.glClearStencil
                PlaybackFuncs.playbackGlClearStencil(cmd)
                return
            when GlFunc.glColorMask
                PlaybackFuncs.playbackGlColorMask(cmd)
                return
            when GlFunc.glCompileShader
                PlaybackFuncs.playbackGlCompileShader(cmd)
                return
            when GlFunc.glCompressedTexImage2D
                PlaybackFuncs.playbackGlCompressedTexImage2D(cmd)
                return
            when GlFunc.glCompressedTexSubImage2D
                PlaybackFuncs.playbackGlCompressedTexSubImage2D(cmd)
                return
            when GlFunc.glCopyTexImage2D
                PlaybackFuncs.playbackGlCopyTexImage2D(cmd)
                return
            when GlFunc.glCopyTexSubImage2D
                PlaybackFuncs.playbackGlCopyTexSubImage2D(cmd)
                return
            when GlFunc.glCreateProgram
                PlaybackFuncs.playbackGlCreateProgram(cmd)
                return
            when GlFunc.glCreateShader
                PlaybackFuncs.playbackGlCreateShader(cmd)
                return
            when GlFunc.glCullFace
                PlaybackFuncs.playbackGlCullFace(cmd)
                return
            when GlFunc.glDeleteBuffers
                PlaybackFuncs.playbackGlDeleteBuffers(cmd)
                return
            when GlFunc.glDeleteFramebuffers
                PlaybackFuncs.playbackGlDeleteFramebuffers(cmd)
                return
            when GlFunc.glDeleteProgram
                PlaybackFuncs.playbackGlDeleteProgram(cmd)
                return
            when GlFunc.glDeleteRenderbuffers
                PlaybackFuncs.playbackGlDeleteRenderbuffers(cmd)
                return
            when GlFunc.glDeleteShader
                PlaybackFuncs.playbackGlDeleteShader(cmd)
                return
            when GlFunc.glDeleteTextures
                PlaybackFuncs.playbackGlDeleteTextures(cmd)
                return
            when GlFunc.glDepthFunc
                PlaybackFuncs.playbackGlDepthFunc(cmd)
                return
            when GlFunc.glDepthMask
                PlaybackFuncs.playbackGlDepthMask(cmd)
                return
            when GlFunc.glDepthRangef
                PlaybackFuncs.playbackGlDepthRangef(cmd)
                return
            when GlFunc.glDetachShader
                PlaybackFuncs.playbackGlDetachShader(cmd)
                return
            when GlFunc.glDisable
                PlaybackFuncs.playbackGlDisable(cmd)
                return
            when GlFunc.glDisableVertexAttribArray
                PlaybackFuncs.playbackGlDisableVertexAttribArray(cmd)
                return
            when GlFunc.glDrawArrays
                PlaybackFuncs.playbackGlDrawArrays(cmd)
                return
            when GlFunc.glDrawElements
                PlaybackFuncs.playbackGlDrawElements(cmd)
                return
            when GlFunc.glEnable
                PlaybackFuncs.playbackGlEnable(cmd)
                return
            when GlFunc.glEnableVertexAttribArray
                PlaybackFuncs.playbackGlEnableVertexAttribArray(cmd)
                return
            when GlFunc.glFinish
                PlaybackFuncs.playbackGlFinish(cmd)
                return
            when GlFunc.glFlush
                PlaybackFuncs.playbackGlFlush(cmd)
                return
            when GlFunc.glFramebufferRenderbuffer
                PlaybackFuncs.playbackGlFramebufferRenderbuffer(cmd)
                return
            when GlFunc.glFramebufferTexture2D
                PlaybackFuncs.playbackGlFramebufferTexture2D(cmd)
                return
            when GlFunc.glFrontFace
                PlaybackFuncs.playbackGlFrontFace(cmd)
                return
            when GlFunc.glGenBuffers
                PlaybackFuncs.playbackGlGenBuffers(cmd)
                return
            when GlFunc.glGenerateMipmap
                PlaybackFuncs.playbackGlGenerateMipmap(cmd)
                return
            when GlFunc.glGenFramebuffers
                PlaybackFuncs.playbackGlGenFramebuffers(cmd)
                return
            when GlFunc.glGenRenderbuffers
                PlaybackFuncs.playbackGlGenRenderbuffers(cmd)
                return
            when GlFunc.glGenTextures
                PlaybackFuncs.playbackGlGenTextures(cmd)
                return
            when GlFunc.glGetActiveAttrib
                PlaybackFuncs.playbackGlGetActiveAttrib(cmd)
                return
            when GlFunc.glGetActiveUniform
                PlaybackFuncs.playbackGlGetActiveUniform(cmd)
                return
            when GlFunc.glGetAttachedShaders
                PlaybackFuncs.playbackGlGetAttachedShaders(cmd)
                return
            when GlFunc.glGetAttribLocation
                PlaybackFuncs.playbackGlGetAttribLocation(cmd)
                return
            when GlFunc.glGetBooleanv
                PlaybackFuncs.playbackGlGetBooleanv(cmd)
                return
            when GlFunc.glGetBufferParameteriv
                PlaybackFuncs.playbackGlGetBufferParameteriv(cmd)
                return
            when GlFunc.glGetError
                PlaybackFuncs.playbackGlGetError(cmd)
                return
            when GlFunc.glGetFloatv
                PlaybackFuncs.playbackGlGetFloatv(cmd)
                return
            when GlFunc.glGetFramebufferAttachmentParameteriv
                PlaybackFuncs.playbackGlGetFramebufferAttachmentParameteriv(cmd)
                return
            when GlFunc.glGetIntegerv
                PlaybackFuncs.playbackGlGetIntegerv(cmd)
                return
            when GlFunc.glGetProgramiv
                PlaybackFuncs.playbackGlGetProgramiv(cmd)
                return
            when GlFunc.glGetProgramInfoLog
                PlaybackFuncs.playbackGlGetProgramInfoLog(cmd)
                return
            when GlFunc.glGetRenderbufferParameteriv
                PlaybackFuncs.playbackGlGetRenderbufferParameteriv(cmd)
                return
            when GlFunc.glGetShaderiv
                PlaybackFuncs.playbackGlGetShaderiv(cmd)
                return
            when GlFunc.glGetShaderInfoLog
                PlaybackFuncs.playbackGlGetShaderInfoLog(cmd)
                return
            when GlFunc.glGetShaderPrecisionFormat
                PlaybackFuncs.playbackGlGetShaderPrecisionFormat(cmd)
                return
            when GlFunc.glGetShaderSource
                PlaybackFuncs.playbackGlGetShaderSource(cmd)
                return
            when GlFunc.glGetString
                PlaybackFuncs.playbackGlGetString(cmd)
                return
            when GlFunc.glGetTexParameterfv
                PlaybackFuncs.playbackGlGetTexParameterfv(cmd)
                return
            when GlFunc.glGetTexParameteriv
                PlaybackFuncs.playbackGlGetTexParameteriv(cmd)
                return
            when GlFunc.glGetUniformfv
                PlaybackFuncs.playbackGlGetUniformfv(cmd)
                return
            when GlFunc.glGetUniformiv
                PlaybackFuncs.playbackGlGetUniformiv(cmd)
                return
            when GlFunc.glGetUniformLocation
                PlaybackFuncs.playbackGlGetUniformLocation(cmd)
                return
            when GlFunc.glGetVertexAttribfv
                PlaybackFuncs.playbackGlGetVertexAttribfv(cmd)
                return
            when GlFunc.glGetVertexAttribiv
                PlaybackFuncs.playbackGlGetVertexAttribiv(cmd)
                return
            when GlFunc.glGetVertexAttribPointerv
                PlaybackFuncs.playbackGlGetVertexAttribPointerv(cmd)
                return
            when GlFunc.glHint
                PlaybackFuncs.playbackGlHint(cmd)
                return
            when GlFunc.glIsBuffer
                PlaybackFuncs.playbackGlIsBuffer(cmd)
                return
            when GlFunc.glIsEnabled
                PlaybackFuncs.playbackGlIsEnabled(cmd)
                return
            when GlFunc.glIsFramebuffer
                PlaybackFuncs.playbackGlIsFramebuffer(cmd)
                return
            when GlFunc.glIsProgram
                PlaybackFuncs.playbackGlIsProgram(cmd)
                return
            when GlFunc.glIsRenderbuffer
                PlaybackFuncs.playbackGlIsRenderbuffer(cmd)
                return
            when GlFunc.glIsShader
                PlaybackFuncs.playbackGlIsShader(cmd)
                return
            when GlFunc.glIsTexture
                PlaybackFuncs.playbackGlIsTexture(cmd)
                return
            when GlFunc.glLineWidth
                PlaybackFuncs.playbackGlLineWidth(cmd)
                return
            when GlFunc.glLinkProgram
                PlaybackFuncs.playbackGlLinkProgram(cmd)
                return
            when GlFunc.glPixelStorei
                PlaybackFuncs.playbackGlPixelStorei(cmd)
                return
            when GlFunc.glPolygonOffset
                PlaybackFuncs.playbackGlPolygonOffset(cmd)
                return
            when GlFunc.glReadPixels
                PlaybackFuncs.playbackGlReadPixels(cmd)
                return
            when GlFunc.glReleaseShaderCompiler
                PlaybackFuncs.playbackGlReleaseShaderCompiler(cmd)
                return
            when GlFunc.glRenderbufferStorage
                PlaybackFuncs.playbackGlRenderbufferStorage(cmd)
                return
            when GlFunc.glSampleCoverage
                PlaybackFuncs.playbackGlSampleCoverage(cmd)
                return
            when GlFunc.glScissor
                PlaybackFuncs.playbackGlScissor(cmd)
                return
            when GlFunc.glShaderBinary
                PlaybackFuncs.playbackGlShaderBinary(cmd)
                return
            when GlFunc.glShaderSource
                PlaybackFuncs.playbackGlShaderSource(cmd)
                return
            when GlFunc.glStencilFunc
                PlaybackFuncs.playbackGlStencilFunc(cmd)
                return
            when GlFunc.glStencilFuncSeparate
                PlaybackFuncs.playbackGlStencilFuncSeparate(cmd)
                return
            when GlFunc.glStencilMask
                PlaybackFuncs.playbackGlStencilMask(cmd)
                return
            when GlFunc.glStencilMaskSeparate
                PlaybackFuncs.playbackGlStencilMaskSeparate(cmd)
                return
            when GlFunc.glStencilOp
                PlaybackFuncs.playbackGlStencilOp(cmd)
                return
            when GlFunc.glStencilOpSeparate
                PlaybackFuncs.playbackGlStencilOpSeparate(cmd)
                return
            when GlFunc.glTexImage2D
                PlaybackFuncs.playbackGlTexImage2D(cmd)
                return
            when GlFunc.glTexParameterf
                PlaybackFuncs.playbackGlTexParameterf(cmd)
                return
            when GlFunc.glTexParameterfv
                PlaybackFuncs.playbackGlTexParameterfv(cmd)
                return
            when GlFunc.glTexParameteri
                PlaybackFuncs.playbackGlTexParameteri(cmd)
                return
            when GlFunc.glTexParameteriv
                PlaybackFuncs.playbackGlTexParameteriv(cmd)
                return
            when GlFunc.glTexSubImage2D
                PlaybackFuncs.playbackGlTexSubImage2D(cmd)
                return
            when GlFunc.glUniform1f
                PlaybackFuncs.playbackGlUniform1f(cmd)
                return
            when GlFunc.glUniform1fv
                PlaybackFuncs.playbackGlUniform1fv(cmd)
                return
            when GlFunc.glUniform1i
                PlaybackFuncs.playbackGlUniform1i(cmd)
                return
            when GlFunc.glUniform1iv
                PlaybackFuncs.playbackGlUniform1iv(cmd)
                return
            when GlFunc.glUniform2f
                PlaybackFuncs.playbackGlUniform2f(cmd)
                return
            when GlFunc.glUniform2fv
                PlaybackFuncs.playbackGlUniform2fv(cmd)
                return
            when GlFunc.glUniform2i
                PlaybackFuncs.playbackGlUniform2i(cmd)
                return
            when GlFunc.glUniform2iv
                PlaybackFuncs.playbackGlUniform2iv(cmd)
                return
            when GlFunc.glUniform3f
                PlaybackFuncs.playbackGlUniform3f(cmd)
                return
            when GlFunc.glUniform3fv
                PlaybackFuncs.playbackGlUniform3fv(cmd)
                return
            when GlFunc.glUniform3i
                PlaybackFuncs.playbackGlUniform3i(cmd)
                return
            when GlFunc.glUniform3iv
                PlaybackFuncs.playbackGlUniform3iv(cmd)
                return
            when GlFunc.glUniform4f
                PlaybackFuncs.playbackGlUniform4f(cmd)
                return
            when GlFunc.glUniform4fv
                PlaybackFuncs.playbackGlUniform4fv(cmd)
                return
            when GlFunc.glUniform4i
                PlaybackFuncs.playbackGlUniform4i(cmd)
                return
            when GlFunc.glUniform4iv
                PlaybackFuncs.playbackGlUniform4iv(cmd)
                return
            when GlFunc.glUniformMatrix2fv
                PlaybackFuncs.playbackGlUniformMatrix2fv(cmd)
                return
            when GlFunc.glUniformMatrix3fv
                PlaybackFuncs.playbackGlUniformMatrix3fv(cmd)
                return
            when GlFunc.glUniformMatrix4fv
                PlaybackFuncs.playbackGlUniformMatrix4fv(cmd)
                return
            when GlFunc.glUseProgram
                PlaybackFuncs.playbackGlUseProgram(cmd)
                return
            when GlFunc.glValidateProgram
                PlaybackFuncs.playbackGlValidateProgram(cmd)
                return
            when GlFunc.glVertexAttrib1f
                PlaybackFuncs.playbackGlVertexAttrib1f(cmd)
                return
            when GlFunc.glVertexAttrib1fv
                PlaybackFuncs.playbackGlVertexAttrib1fv(cmd)
                return
            when GlFunc.glVertexAttrib2f
                PlaybackFuncs.playbackGlVertexAttrib2f(cmd)
                return
            when GlFunc.glVertexAttrib2fv
                PlaybackFuncs.playbackGlVertexAttrib2fv(cmd)
                return
            when GlFunc.glVertexAttrib3f
                PlaybackFuncs.playbackGlVertexAttrib3f(cmd)
                return
            when GlFunc.glVertexAttrib3fv
                PlaybackFuncs.playbackGlVertexAttrib3fv(cmd)
                return
            when GlFunc.glVertexAttrib4f
                PlaybackFuncs.playbackGlVertexAttrib4f(cmd)
                return
            when GlFunc.glVertexAttrib4fv
                PlaybackFuncs.playbackGlVertexAttrib4fv(cmd)
                return
            when GlFunc.glVertexAttribPointer
                PlaybackFuncs.playbackGlVertexAttribPointer(cmd)
                return
            when GlFunc.glViewport
                PlaybackFuncs.playbackGlViewport(cmd)
                return
            default
                //assert(false)
        return

    def static startPlayback(recordingFile : string)
        if Recorder.recorderState == RecorderModuleState.playbackMedia
            playbackFileName = recordingFile
            loadInstructions()
            playbackStarted = true

    def static playUntilFunction(func : GlFunc)
        if (Recorder.recorderState == RecorderModuleState.playbackMedia) and playbackStarted
            Thread.usleep(20000)
            while true
                breakAtEnd : bool = false
                if instructionBuff[instructionIdx].command == func
                    instructionIdx++
                    numberOfFramesPlayed++
                    print "hit a break condition"
                    breakAtEnd = true
                else
                    executeInstruction(instructionBuff[instructionIdx])
                    print "playing back " + instructionBuff[instructionIdx].command.to_string()
                    var err = GL.raw_glGetError()
                    if err != 0
                        print "glerror: " + err.to_string() + " " + instructionBuff[instructionIdx].command.to_string()
                    instructionIdx++
                    numberOfFramesPlayed++
                if instructionIdx == instructionBuff.size
                    loadInstructions()
                    if instructionBuff.size == 0
                        print "finished playback"
                        playbackFinished = true
                    break
                if breakAtEnd
                    break
