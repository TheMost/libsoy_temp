/*
 *  libsoy - soy.recording.PointerSerializer
 *  Copyright (C) 2006-2015 Copyleft Games Group
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, see http://www.gnu.org/licenses
 *
 */

[indent=4]
uses GL

class soy.recording.PointerSerializer : Object

    def static private serializeArray(pointer : string, numBytes : long) : string 
        var dataPtr = (char*)(long.parse(pointer))
        
        var sb = new StringBuilder.sized((size_t)(numBytes))

        i : int
        var arr = new array of uchar[numBytes]
        for i = 0 to (numBytes-1)
            arr[i] = (uchar)dataPtr[i]
            var aaab = dataPtr[i]
            var aaad = dataPtr[i].to_string()
            sb.append(dataPtr[i].to_string())
        var intermediate = sb.str.to_utf8()
        var ll = sb.str.length
        var l = intermediate.length
        var testt = sb.str.data.length
        return Base64.encode(arr)

    def static private readBytesFromString(str : string, numBytes : long) : array of char
        var decodedStr = Base64.decode(str)
        var ret = new array of char[numBytes]
        assert(numBytes == decodedStr.length)
        i : int
        for i = 0 to (numBytes-1)
            ret[i] = (char)decodedStr[i]
        return ret

    def static serializeglBufferData( parameters : string) : string
        var splitParams = parameters.split(" ")
        var data = serializeArray(splitParams[2], long.parse(splitParams[1]))
        //timer.stop()
        //ms : ulong
        //var seconds = timer.elapsed(out ms)
        return splitParams[0] + " " + splitParams[1] + " " + data + " " + splitParams[3] 

    def static serializeglBufferSubData( parameters : string) : string
        var splitParams = parameters.split(" ")
        var data = serializeArray(splitParams[3], long.parse(splitParams[2]))
        return splitParams[0] + " " + splitParams[1] + " " + splitParams[2] + " " + data 

    def static deserializeglBufferData(cmd : EncodedCmd, out ret : array of char) 
        var numBytes = long.parse(cmd.parameters[1])
        ret = readBytesFromString(cmd.parameters[2], numBytes)

    def static serializeglCompressedTexImage2D( parameters : string) : string
        var splitParams = parameters.split(" ")
        var data = serializeArray(splitParams[7], long.parse(splitParams[6]))
        return splitParams[0] + " " + splitParams[1] + " " + splitParams[2] + " " + splitParams[3] + " " + splitParams[4] + " " + splitParams[5] + " " + splitParams[6] + " " +  data 

    def static deserializeglCompressedTexImage2D ( cmd : EncodedCmd) : GLvoid*
        var numBytes = long.parse(cmd.parameters[6])
        var data = readBytesFromString(cmd.parameters[7], numBytes)
        return (GLvoid*)data

    def static serializeglCompressedTexSubImage2D( parameters : string) : string
        var splitParams = parameters.split(" ")
        var data = serializeArray(splitParams[8], long.parse(splitParams[7]))
        return splitParams[0] + " " + splitParams[1] + " " + splitParams[2] + " " + splitParams[3] + " " + splitParams[4] + " " + splitParams[5] + " " + splitParams[6] + " " +  splitParams[7] + " " + data 

    def static deserializeglCompressedTexSubImage2D(cmd : EncodedCmd) : GLvoid*
        var numBytes = long.parse(cmd.parameters[7])
        var data = readBytesFromString(cmd.parameters[8],numBytes)
        return (GLvoid*)data

    def static serializeglDrawElements( parameters : string) : string
        return parameters

    def static serializeglGetVertexAttribPointerv( parameters : string) : string
        return "unimplemented"

    def static serializeglReadPixels( parameters : string) : string
        var splitParams = parameters.split(" ")
        var width = long.parse(splitParams[2])
        var height = long.parse(splitParams[3])
        var size = GLMetaFuncs.getGLDatatypeSize(int.parse(splitParams[5]))
        var data = serializeArray(splitParams[6], size * width * height)

        return splitParams[0] + " " + splitParams[1] + " " + splitParams[2] + " " + splitParams[3] + " " + splitParams[4] + " " + splitParams[5] + " " + data

    def static serializeglShaderBinary( parameters : string) : string
        return "unimplemented"

    def static serializeglShaderSource( parameters : string) : string
        var splitParams = parameters.split(" ")
        return parameters

    def static deserializeglShaderSource( cmd : EncodedCmd, out str : GLchar**, out len : GLint*) 
        var shader = Base64.decode(cmd.parameters[3])
        var shaderCount = int.parse(cmd.parameters[1])
        len = new array of GLint[shaderCount]
        i : int
        for i=0 to (shaderCount-1)
            len[i] = 0
        str = (GLchar**)shader
        return



    def static serializeglTexImage2D( parameters : string) : string
        var splitParams = parameters.split(" ")
        var data = "null"

        if splitParams[8] != "0"
            var format = int.parse(splitParams[7])
            var dataSize = GLMetaFuncs.getGLDatatypeSize(format) 

            data = serializeArray(splitParams[8], dataSize * long.parse(splitParams[3]) * long.parse(splitParams[4]) * 4 )

        return splitParams[0] + " " +  splitParams[1] + " " + splitParams[2] + " " + splitParams[3] + " " + splitParams[4] + " " + splitParams[5] + " " + splitParams[6] + " " + splitParams[7] + " " + data

    def static deserializeglTexImage2D(cmd : EncodedCmd, out ret : array of char) 
        var wordSize = GLMetaFuncs.getGLDatatypeSize(int.parse(cmd.parameters[7]))
        var width = long.parse(cmd.parameters[3])
        var height = long.parse(cmd.parameters[4])
        var numBytes = width * height * wordSize * 4
        data : char* 
        if cmd.parameters[8] == "null"
            ret = new array of char[0]  
        else
            ret = readBytesFromString(cmd.parameters[8],numBytes)
            var ttt =  readBytesFromString(cmd.parameters[8],numBytes)
            ret = ttt
            var a=5
        



    def static serializeglTexSubImage2D( parameters : string) : string
        var splitParams = parameters.split(" ")
        var data = "null"

        var format = int.parse(splitParams[7])
        var dataSize = GLMetaFuncs.getGLDatatypeSize(format)*4 

        data = serializeArray(splitParams[8], dataSize * long.parse(splitParams[3]) * long.parse(splitParams[4]))

        return splitParams[0] + " " +  splitParams[1] + " " + splitParams[2] + " " + splitParams[3] + " " + splitParams[4] + " " + splitParams[5] + " " + splitParams[6] + " " + splitParams[7] + " " + data

    def static deserializeglTexSubImage2D(cmd : EncodedCmd) : GLvoid*
        var wordSize = GLMetaFuncs.getGLDatatypeSize(int.parse(cmd.parameters[7]))
        var width = long.parse(cmd.parameters[4])
        var height = long.parse(cmd.parameters[5])
        var data = readBytesFromString(cmd.parameters[8],width * height * wordSize)
        return (GLvoid*)data

    def static serializeglUniform1fv( parameters : string) : string
        return "unimplemented"

    def static serializeglUniform1iv( parameters : string) : string
        var splitParams = parameters.split(" ")
        var data = serializeArray(splitParams[2], long.parse(splitParams[1]) * 4)
        return splitParams[0] + " " + splitParams[1] + " " +  data 

    def static deserializeglUniform1iv ( cmd : EncodedCmd, out ret : array of char)
        var dataLenBytes = long.parse(cmd.parameters[1])
        ret = readBytesFromString(cmd.parameters[2], dataLenBytes * 4)

    def static serializeglUniform2fv( parameters : string) : string
        return "unimplemented"
    def static serializeglUniform2iv( parameters : string) : string
        return "unimplemented"

    def static serializeglUniform3fv( parameters : string) : string
        var splitParams = parameters.split(" ")
        var data = serializeArray(splitParams[2], long.parse(splitParams[1]) * 4 * 3)
        return splitParams[0] + " " + splitParams[1] + " " +  data 

    def static deserializeglUniform3fv ( cmd : EncodedCmd, out ret : array of char) 
        var dataLenBytes = long.parse(cmd.parameters[1])
        ret = readBytesFromString(cmd.parameters[2], dataLenBytes * 4 * 3)

    def static serializeglUniform3iv( parameters : string) : string
        return "unimplemented"

    def static serializeglUniform4fv( parameters : string) : string
        var splitParams = parameters.split(" ")
        var data = serializeArray(splitParams[2], long.parse(splitParams[1]) * 4 * 4)
        return splitParams[0] + " " + splitParams[1] + " " +  data 

    def static deserializeglUniform4fv ( cmd : EncodedCmd, out ret : array of char)
        var dataLenBytes = long.parse(cmd.parameters[1])
        ret = readBytesFromString(cmd.parameters[2], dataLenBytes * 4 * 4)

    def static serializeglUniform4iv( parameters : string) : string
        return "unimplemented"
    def static serializeglUniformMatrix2fv( parameters : string) : string
        return "unimplemented"
    def static serializeglUniformMatrix3fv( parameters : string) : string
        return "unimplemented"
    
    def static serializeglUniformMatrix4fv( parameters : string) : string
        var splitParams = parameters.split(" ")
        var data = serializeArray(splitParams[3], long.parse(splitParams[1]) * 4 * 16)
        return splitParams[0] + " " + splitParams[1] + " " +  splitParams[2] + " " + data 

    def static deserializeglUniformMatrix4fv( cmd : EncodedCmd, out ret : array of char) 
        var numElements = long.parse(cmd.parameters[1])
        ret = readBytesFromString(cmd.parameters[3], numElements *  4 * 16)

    def static serializeglVertexAttrib1fv( parameters : string) : string
        return "unimplemented"
    def static serializeglVertexAttrib2fv( parameters : string) : string
        return "unimplemented"
    def static serializeglVertexAttrib3fv( parameters : string) : string
        return "unimplemented"
    def static serializeglVertexAttrib4fv( parameters : string) : string
        return "unimplemented"


    def static serializeDataFunction ( funcName : GlFunc, parameters : string) : string
        case funcName
            when GlFunc.glBufferData
                return serializeglBufferData(parameters)
            when GlFunc.glBufferSubData
                return serializeglBufferSubData(parameters)
            when GlFunc.glCompressedTexImage2D
                return serializeglCompressedTexImage2D(parameters)
            when GlFunc.glCompressedTexSubImage2D
                return serializeglCompressedTexSubImage2D(parameters)
            when GlFunc.glDrawElements
                return serializeglDrawElements(parameters)
            when GlFunc.glGetVertexAttribPointerv
                return serializeglGetVertexAttribPointerv(parameters)
            when GlFunc.glReadPixels
                return serializeglReadPixels(parameters)
            when GlFunc.glShaderBinary
                return serializeglShaderBinary(parameters)
            when GlFunc.glShaderSource
                return serializeglShaderSource(parameters)
            when GlFunc.glTexImage2D
                return serializeglTexImage2D(parameters)
            when GlFunc.glTexSubImage2D
                return serializeglTexSubImage2D(parameters)
            when GlFunc.glUniform1fv
                return serializeglUniform1fv(parameters)
            when GlFunc.glUniform1iv
                return serializeglUniform1iv(parameters)
            when GlFunc.glUniform2fv
                return serializeglUniform2fv(parameters)
            when GlFunc.glUniform2iv
                return serializeglUniform2iv(parameters)
            when GlFunc.glUniform3fv
                return serializeglUniform3fv(parameters)
            when GlFunc.glUniform3iv
                return serializeglUniform3iv(parameters)
            when GlFunc.glUniform4fv
                return serializeglUniform4fv(parameters)
            when GlFunc.glUniform4iv
                return serializeglUniform4iv(parameters)
            when GlFunc.glUniformMatrix2fv
                return serializeglUniformMatrix2fv(parameters)
            when GlFunc.glUniformMatrix3fv
                return serializeglUniformMatrix3fv(parameters)
            when GlFunc.glUniformMatrix4fv
                return serializeglUniformMatrix4fv(parameters)
            when GlFunc.glVertexAttrib1fv
                return serializeglVertexAttrib1fv(parameters)
            when GlFunc.glVertexAttrib2fv
                return serializeglVertexAttrib2fv(parameters)
            when GlFunc.glVertexAttrib3fv
                return serializeglVertexAttrib3fv(parameters)
            when GlFunc.glVertexAttrib4fv
                return serializeglVertexAttrib4fv(parameters)
            default
                assert(false)
                return "this shouldn't get hit"






