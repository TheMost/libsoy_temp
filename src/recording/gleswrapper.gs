/*
 *  libsoy - recording.gleswrapper
 *  Copyright (C) 2006-2015 Copyleft Games Group
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, see http://www.gnu.org/licenses
 *
 */

[indent=4]

uses
    GL
    soy.recording

namespace GL

    [CCode (cname = "wrap_glActiveTexture")]
    def glActiveTexture (texture : GLenum)
        Recorder.recordCall(GlFunc.glActiveTexture, texture.to_string())
        GL.raw_glActiveTexture(texture)
        return

    [CCode (cname = "wrap_glAttachShader")]
    def glAttachShader(program : GLuint, index : GLuint)
        Recorder.recordCall(GlFunc.glAttachShader, program.to_string() + " " + index.to_string()) 
        GL.raw_glAttachShader(program, index)
        return

    [CCode (cname = "wrap_glBindAttribLocation")]
    def glBindAttribLocation (program : GLuint, index : GLuint, name : string)
        Recorder.recordCall(GlFunc.glBindAttribLocation, program.to_string() + " " + index.to_string() + " " + name)
        GL.raw_glBindAttribLocation( program, index, name)
        return

    [CCode (cname = "wrap_glBindBuffer")]
    def glBindBuffer (target : GLenum, buffer : GLuint)
        Recorder.recordCall(GlFunc.glBindBuffer, target.to_string() + " " + buffer.to_string()) 
        GL.raw_glBindBuffer(target, buffer )
        return

    [CCode (cname = "wrap_glBindFramebuffer")]
    def glBindFramebuffer (target : GLenum, framebuffer : GLuint)
        Recorder.recordCall(GlFunc.glBindFramebuffer, target.to_string() + " " + framebuffer.to_string()) 
        GL.raw_glBindFramebuffer( target, framebuffer)
        return

    [CCode (cname = "wrap_glBindRenderbuffer")]
    def glBindRenderbuffer (target : GLenum, renderbuffer : GLuint)
        Recorder.recordCall(GlFunc.glBindRenderbuffer,  target.to_string() + " " + renderbuffer.to_string()) 
        GL.raw_glBindRenderbuffer( target, renderbuffer)
        return

    [CCode (cname = "wrap_glBindTexture")]
    def glBindTexture (target : GLenum, texture : GLuint)
        Recorder.recordCall(GlFunc.glBindTexture, target.to_string() + " " + texture.to_string()) 
        GL.raw_glBindTexture(target, texture )
        return

    [CCode (cname = "wrap_glBlendColor")]
    def glBlendColor (red : GLclampf, green : GLclampf, blue : GLclampf, alpha : GLclampf)
        Recorder.recordCall(GlFunc.glBlendColor, red.to_string() + " " + green.to_string() + " " + blue.to_string() + " " + alpha.to_string()) 
        GL.raw_glBlendColor( red, green, blue, alpha)
        return

    [CCode (cname = "wrap_glBlendEquation")]
    def glBlendEquation (mode : GLenum)
        Recorder.recordCall(GlFunc.glBlendEquation, mode.to_string())
        GL.raw_glBlendEquation(mode )
        return

    [CCode (cname = "wrap_glBlendEquationSeparate")]
    def glBlendEquationSeparate (modeRGB : GLenum, modeAlpha : GLenum)
        Recorder.recordCall(GlFunc.glBlendEquationSeparate, modeRGB.to_string() + " " + modeAlpha.to_string())
        GL.raw_glBlendEquationSeparate( modeRGB, modeAlpha)
        return

    [CCode (cname = "wrap_glBlendFunc")]
    def glBlendFunc (sfactor : GLenum, dfactor : GLenum)
        Recorder.recordCall(GlFunc.glBlendFunc, sfactor.to_string() + " " + dfactor.to_string()) 
        GL.raw_glBlendFunc(sfactor, dfactor )
        return

    [CCode (cname = "wrap_glBlendFuncSeparate")]
    def glBlendFuncSeparate (srcRGB : GLenum, dstRGB : GLenum, srcAlpha : GLenum, dstAlpha : GLenum)
        Recorder.recordCall(GlFunc.glBlendFuncSeparate, srcRGB.to_string() + " " + dstRGB.to_string() + " " + srcAlpha.to_string() + " " + dstAlpha.to_string())
        GL.raw_glBlendFuncSeparate(srcRGB, dstRGB, srcAlpha, dstAlpha )
        return

    [CCode (cname = "wrap_glBufferData")]
    def glBufferData (target : GLenum, size : GLsizeiptr, data : GLvoid*, usage : GLenum)
        Recorder.recordPointerCall(GlFunc.glBufferData, target.to_string() + " " + size.to_string() + " " + ((long)data).to_string() + " " + ((long)usage).to_string())
        GL.raw_glBufferData( target, size, data, usage)
        return

    [CCode (cname = "wrap_glBufferSubData")]
    def glBufferSubData (target : GLenum, offset : GLintptr, size : GLsizeiptr, data : GLvoid*)
        Recorder.recordPointerCall(GlFunc.glBufferSubData, target.to_string() + " " + offset.to_string() + " " + size.to_string() + " " + ((long)data).to_string()) 
        GL.raw_glBufferSubData( target, offset, size, data)
        return

    [CCode (cname = "wrap_glCheckFramebufferStatus")]
    def glCheckFramebufferStatus (target : GLenum) : GLenum
        Recorder.recordCall(GlFunc.glCheckFramebufferStatus, target.to_string()) 
        return GL.raw_glCheckFramebufferStatus(target)

    [CCode (cname = "wrap_glClear")]
    def glClear (mask : GLbitfield)
        Recorder.recordCall(GlFunc.glClear, mask.to_string()) 
        GL.raw_glClear( mask)
        return

    [CCode (cname = "wrap_glClearColor")]
    def glClearColor (red : GLclampf, green : GLclampf, blue : GLclampf, alpha : GLclampf)
        Recorder.recordCall(GlFunc.glClearColor, red.to_string() + " " + green.to_string() + " " + blue.to_string() + " " + alpha.to_string()) 
        GL.raw_glClearColor(red, green, blue, alpha)
        return

    [CCode (cname = "wrap_glClearDepthf")]
    def glClearDepthf (depth : GLclampf)
        Recorder.recordCall(GlFunc.glClearDepthf,depth.to_string()) 
        GL.raw_glClearDepthf( depth)
        return

    [CCode (cname = "wrap_glClearStencil")]
    def glClearStencil (s : GLint)
        Recorder.recordCall(GlFunc.glClearStencil,s.to_string()) 
        GL.raw_glClearStencil( s)
        return

    [CCode (cname = "wrap_glColorMask")]
    def glColorMask (red : GLboolean, green : GLboolean, blue : GLboolean, alpha  : GLboolean)
        Recorder.recordCall(GlFunc.glColorMask,  red.to_string() + " " + green.to_string() + " " + blue.to_string() + " " + alpha.to_string()) 
        GL.raw_glColorMask( red, green, blue, alpha)
        return

    [CCode (cname = "wrap_glCompileShader")]
    def glCompileShader (shader : GLuint)
        Recorder.recordCall(GlFunc.glCompileShader,shader.to_string()) 
        GL.raw_glCompileShader( shader)
        return

    [CCode (cname = "wrap_glCompressedTexImage2D")]
    def glCompressedTexImage2D (target : GLenum, level : GLint, internalformat : GLenum, width : GLsizei, height : GLsizei, border : GLint, imageSize : GLsizei, data : GLvoid*)
        Recorder.recordPointerCall(GlFunc.glCompressedTexImage2D,target.to_string() + " " +  level.to_string()  + " " + internalformat.to_string() + " "  + width.to_string() + " " + height.to_string() + " " +  border.to_string() + imageSize.to_string() + ((long)data).to_string())
        GL.raw_glCompressedTexImage2D( target, level, internalformat, width, height, border, imageSize, data)
        return

    [CCode (cname = "wrap_glCompressedTexSubImage2D")]
    def glCompressedTexSubImage2D (target : GLenum, level : GLint, xoffset : GLint, yoffset : GLint, width : GLsizei, height : GLsizei, format : GLenum, imageSize : GLsizei, data : GLvoid*)
        Recorder.recordPointerCall(GlFunc.glCompressedTexSubImage2D, target.to_string() + " " + level.to_string() + " " + xoffset.to_string() + " " + yoffset.to_string() + " " + width.to_string() + " " + height.to_string() + " " + format.to_string() + " " + imageSize.to_string() + " " + ((long)data).to_string()) 
        GL.raw_glCompressedTexSubImage2D(target, level, xoffset, yoffset, width, height, format, imageSize, data )
        return

    [CCode (cname = "wrap_glCopyTexImage2D")]
    def glCopyTexImage2D (target : GLenum, level : GLint, internalformat : GLenum, x : GLint, y : GLint, width : GLsizei, height : GLsizei, border : GLint)
        Recorder.recordCall(GlFunc.glCopyTexImage2D, target.to_string() + " " + level.to_string() + " " + internalformat.to_string() + " " + x.to_string() + " " + y.to_string() + " " + width.to_string() + " " + height.to_string() + " " + border.to_string()) 
        GL.raw_glCopyTexImage2D( target, level, internalformat, x, y, width, height, border)
        return

    [CCode (cname = "wrap_glCopyTexSubImage2D")]
    def glCopyTexSubImage2D (target : GLenum, level : GLint, xoffset : GLint, yoffset : GLint, x : GLint, y : GLint, width : GLsizei, height : GLsizei)
        Recorder.recordCall(GlFunc.glCopyTexSubImage2D, target.to_string() + " " + level.to_string() + " " + xoffset.to_string() + " " + yoffset.to_string() + " " + x.to_string() + " " + y.to_string() + " " + width.to_string() + " " + height.to_string()) 
        GL.raw_glCopyTexSubImage2D( target, level, xoffset, yoffset, x, y, width, height)
        return

    [CCode (cname = "wrap_glCreateProgram")]
    def glCreateProgram ( ) : GLuint
        Recorder.recordCall(GlFunc.glCreateProgram, "") 
        return GL.raw_glCreateProgram()

    [CCode (cname = "wrap_glCreateShader")]
    def glCreateShader (type : GLenum) : GLuint
        Recorder.recordCall(GlFunc.glCreateShader, type.to_string()) 
        return GL.raw_glCreateShader(type)

    [CCode (cname = "wrap_glCullFace")]
    def glCullFace (mode : GLenum)
        Recorder.recordCall(GlFunc.glCullFace, mode.to_string()) 
        GL.raw_glCullFace( mode)
        return

    [CCode (cname = "wrap_glDeleteBuffers")]
    def glDeleteBuffers (buffers : array of GLuint)
        Recorder.recordCall(GlFunc.glDeleteBuffers, TypeSerializer.serializeGLuintArr(buffers)) 
        GL.raw_glDeleteBuffers( buffers)
        return

    [CCode (cname = "wrap_glDeleteFramebuffers")]
    def glDeleteFramebuffers (framebuffers : array of GLuint)
        Recorder.recordCall(GlFunc.glDeleteFramebuffers,  TypeSerializer.serializeGLuintArr(framebuffers)) 
        GL.raw_glDeleteFramebuffers( framebuffers)
        return

    [CCode (cname = "wrap_glDeleteProgram")]
    def glDeleteProgram (program : GLuint)
        Recorder.recordCall(GlFunc.glDeleteProgram, program.to_string()) 
        GL.raw_glDeleteProgram( program)
        return

    [CCode (cname = "wrap_glDeleteRenderbuffers")]
    def glDeleteRenderbuffers (renderbuffers : array of GLuint)
        Recorder.recordCall(GlFunc.glDeleteRenderbuffers,  TypeSerializer.serializeGLuintArr(renderbuffers)) 
        GL.raw_glDeleteRenderbuffers(renderbuffers )
        return

    [CCode (cname = "wrap_glDeleteShader")]
    def glDeleteShader (shader : GLuint)
        Recorder.recordCall(GlFunc.glDeleteShader, shader.to_string()) 
        GL.raw_glDeleteShader( shader)
        return

    [CCode (cname = "wrap_glDeleteTextures")]
    def glDeleteTextures (textures : array of GLuint)
        Recorder.recordCall(GlFunc.glDeleteTextures, TypeSerializer.serializeGLuintArr(textures)) 
        GL.raw_glDeleteTextures(textures )
        return

    [CCode (cname = "wrap_glDepthFunc")]
    def glDepthFunc (func : GLenum)
        Recorder.recordCall(GlFunc.glDepthFunc, func.to_string()) 
        GL.raw_glDepthFunc( func)
        return

    [CCode (cname = "wrap_glDepthMask")]
    def glDepthMask (flag : GLboolean)
        Recorder.recordCall(GlFunc.glDepthMask, flag.to_string()) 
        GL.raw_glDepthMask( flag)
        return

    [CCode (cname = "wrap_glDepthRangef")]
    def glDepthRangef (zNear : GLclampf, zFar : GLclampf)
        Recorder.recordCall(GlFunc.glDepthRangef, zNear.to_string() + " " + zFar.to_string()) 
        GL.raw_glDepthRangef( zNear, zFar)
        return

    [CCode (cname = "wrap_glDetachShader")]
    def glDetachShader (program : GLuint, shader : GLuint)
        Recorder.recordCall(GlFunc.glDetachShader, program.to_string() + " " + shader.to_string()) 
        GL.raw_glDetachShader( program, shader)
        return

    [CCode (cname = "wrap_glDisable")]
    def glDisable (cap : GLenum)
        Recorder.recordCall(GlFunc.glDisable, cap.to_string()) 
        GL.raw_glDisable( cap)
        return

    [CCode (cname = "wrap_glDisableVertexAttribArray")]
    def glDisableVertexAttribArray (index : GLuint)
        Recorder.recordCall(GlFunc.glDisableVertexAttribArray, index.to_string()) 
        GL.raw_glDisableVertexAttribArray(index )
        return

    [CCode (cname = "wrap_glDrawArrays")]
    def glDrawArrays (mode : GLenum, first : GLint , count : GLsizei)
        Recorder.recordCall(GlFunc.glDrawArrays, mode.to_string() + " " + first.to_string() + " " + count.to_string()) 
        GL.raw_glDrawArrays(mode, first, count )
        return

    [CCode (cname = "wrap_glDrawElements")]
    def glDrawElements (mode : GLenum, count : GLsizei, type : GLenum, indicies : GLvoid*)
        Recorder.recordPointerCall(GlFunc.glDrawElements, mode.to_string() + " " + count.to_string() + " " + type.to_string() + " " + ((long)indicies).to_string()) 
        GL.raw_glDrawElements(mode, count, type, indicies )
        return

    [CCode (cname = "wrap_glEnable")]
    def glEnable (cap : GLenum)
        Recorder.recordCall(GlFunc.glEnable,cap.to_string()) 
        GL.raw_glEnable( cap)
        return

    [CCode (cname = "wrap_glEnableVertexAttribArray")]
    def glEnableVertexAttribArray (index : GLuint)
        Recorder.recordCall(GlFunc.glEnableVertexAttribArray, index.to_string()) 
        GL.raw_glEnableVertexAttribArray( index)
        return

    [CCode (cname = "wrap_glFinish")]
    def glFinish ( )
        Recorder.recordCall(GlFunc.glFinish, "") 
        GL.raw_glFinish( )
        return

    [CCode (cname = "wrap_glFlush")]
    def glFlush ( )
        Recorder.recordCall(GlFunc.glFlush, "")
        GL.raw_glFlush( )
        return

    [CCode (cname = "wrap_glFramebufferRenderbuffer")]
    def glFramebufferRenderbuffer (target : GLenum, attachment : GLenum, renderbuffertarget : GLenum, renderbuffer : GLuint)
        Recorder.recordCall(GlFunc.glFramebufferRenderbuffer, target.to_string() + " " + attachment.to_string() + " " + renderbuffertarget.to_string() + " " + renderbuffer.to_string()) 
        GL.raw_glFramebufferRenderbuffer(target, attachment, renderbuffertarget, renderbuffer )
        return

    [CCode (cname = "wrap_glFramebufferTexture2D")]
    def glFramebufferTexture2D (target : GLenum, attachment : GLenum, textarget : GLenum, texture : GLuint, level : GLint)
        Recorder.recordCall(GlFunc.glFramebufferTexture2D, target.to_string() + " " + attachment.to_string() + " " + textarget.to_string() + " " + texture.to_string() + " " + level.to_string()) 
        GL.raw_glFramebufferTexture2D( target, attachment, textarget, texture, level)
        return

    [CCode (cname = "wrap_glFrontFace")]
    def glFrontFace (mode : GLenum)
        Recorder.recordCall(GlFunc.glFrontFace, mode.to_string()) 
        GL.raw_glFrontFace( mode)
        return

    [CCode (cname = "wrap_glGenBuffers")]
    def glGenBuffers (buffers : array of GLuint)
        Recorder.recordCall(GlFunc.glGenBuffers, TypeSerializer.serializeGLuintArr(buffers)) 
        GL.raw_glGenBuffers( buffers)
        return

    [CCode (cname = "wrap_glGenerateMipmap")]
    def glGenerateMipmap (target : GLenum)
        Recorder.recordCall(GlFunc.glGenerateMipmap, target.to_string()) 
        GL.raw_glGenerateMipmap( target)
        return

    [CCode (cname = "wrap_glGenFramebuffers")]
    def glGenFramebuffers (framebuffers : array of GLuint)
        Recorder.recordCall(GlFunc.glGenFramebuffers, TypeSerializer.serializeGLuintArr(framebuffers))
        GL.raw_glGenFramebuffers(framebuffers )
        return

    [CCode (cname = "wrap_glGenRenderbuffers")]
    def glGenRenderbuffers (renderbuffers : array of GLuint)
        Recorder.recordCall(GlFunc.glGenRenderbuffers,  renderbuffers.length.to_string())//TypeSerializer.serializeGLuintArr(renderbuffers))
        GL.raw_glGenRenderbuffers( renderbuffers)
        return

    [CCode (cname = "wrap_glGenTextures")]
    def glGenTextures (textures : array of GLuint)
        Recorder.recordCall(GlFunc.glGenTextures, TypeSerializer.serializeGLuintArr(textures)) 
        GL.raw_glGenTextures(textures )
        return

    [CCode (cname = "wrap_glGetActiveAttrib")]
    def glGetActiveAttrib (program : GLuint, index : GLuint, bufsize : GLsizei, out length : GLsizei, out size : GLint, out type : GLenum, name : string)
        Recorder.recordCall(GlFunc.glGetActiveAttrib,program.to_string() + " " + index.to_string() + " " + bufsize.to_string() + " " + length.to_string() + " " + size.to_string() + " " + type.to_string() + " " + name.to_string()) 
        GL.raw_glGetActiveAttrib(program, index, bufsize, out length, out size, out type, name )
        return

    [CCode (cname = "wrap_glGetActiveUniform")]
    def glGetActiveUniform (program : GLuint, index : GLuint, bufsize : GLsizei, out length : GLsizei, out size : GLint, out type : GLenum, name : string)
        Recorder.recordCall(GlFunc.glGetActiveUniform, program.to_string() + " " + index.to_string() + " " + bufsize.to_string() + " " + length.to_string() + " " + size.to_string() + " " + type.to_string() + " " + name.to_string()) 
        GL.raw_glGetActiveUniform( program, index, bufsize, out length, out size, out type, name)
        return

    [CCode (cname = "wrap_glGetAttachedShaders")]
    def glGetAttachedShaders (program : GLuint, maxcount : GLsizei, out count : GLsizei, out shaders : GLuint)
        Recorder.recordCall(GlFunc.glGetAttachedShaders, program.to_string() + " " + maxcount.to_string() + " " + count.to_string() + " " + shaders.to_string()) 
        GL.raw_glGetAttachedShaders( program, maxcount, out count, out shaders)
        return

    [CCode (cname = "wrap_glGetAttribLocation")]
    def glGetAttribLocation (program : GLuint, name : string) : int
        Recorder.recordCall(GlFunc.glGetAttribLocation, program.to_string() + " " + name.to_string()) 
        return GL.raw_glGetAttribLocation( program, name)

    [CCode (cname = "wrap_glGetBooleanv")]
    def glGetBooleanv (pname : GLenum, parameters : array of GLboolean)
        Recorder.recordCall(GlFunc.glGetBooleanv, pname.to_string() + " " + TypeSerializer.serializeGLbooleanArr(parameters)) 
        GL.raw_glGetBooleanv( pname, parameters)
        return

    [CCode (cname = "wrap_glGetBufferParameteriv")]
    def glGetBufferParameteriv (target : GLenum, pname : GLenum, out parameters : GLint)
        Recorder.recordCall(GlFunc.glGetBufferParameteriv, target.to_string() + " " + pname.to_string() + " " + parameters.to_string()) 
        GL.raw_glGetBufferParameteriv(target, pname, out parameters )
        return

    [CCode (cname = "wrap_glGetError")]
    def glGetError ( ) : GLenum
        //Recorder.recordCall(GlFunc.glGetError, "") 
        return GL.raw_glGetError()

    [CCode (cname = "wrap_glGetFloatv")]
    def glGetFloatv (pname : GLenum, parameters : array of GLfloat)
        Recorder.recordCall(GlFunc.glGetFloatv, pname.to_string() + " " + TypeSerializer.serializeGLfloatArr(parameters))
        GL.raw_glGetFloatv( pname, parameters)
        return

    [CCode (cname = "wrap_glGetFramebufferAttachmentParameteriv")]
    def glGetFramebufferAttachmentParameteriv (target : GLenum, attachment : GLenum, pname : GLenum, out parameters : GLint)
        Recorder.recordCall(GlFunc.glGetFramebufferAttachmentParameteriv, target.to_string() + " " + attachment.to_string() + " " + pname.to_string() + " " + parameters.to_string()) 
        GL.raw_glGetFramebufferAttachmentParameteriv( target, attachment, pname, out parameters)
        return

    [CCode (cname = "wrap_glGetIntegerv")]
    def glGetIntegerv (pname : GLenum, parameters : array of GLint)
        Recorder.recordCall(GlFunc.glGetIntegerv, pname.to_string() + " " + TypeSerializer.serializeGLintArr(parameters)) 
        GL.raw_glGetIntegerv( pname, parameters)
        return

    [CCode (cname = "wrap_glGetProgramiv")]
    def glGetProgramiv (program : GLuint, pname : GLenum, out parameters : GLint)
        Recorder.recordCall(GlFunc.glGetProgramiv, program.to_string() + " " + pname.to_string() + " " + parameters.to_string()) 
        GL.raw_glGetProgramiv( program, pname, out parameters)
        return

    [CCode (cname = "wrap_glGetProgramInfoLog")]
    def glGetProgramInfoLog (program : GLuint, bufsize : GLsizei, out length : GLsizei, infolog : array of GLchar)
        Recorder.recordCall(GlFunc.glGetProgramInfoLog, program.to_string() + " " + bufsize.to_string() + " " + length.to_string() + " " + TypeSerializer.serializeGLcharArr(infolog))
        GL.raw_glGetProgramInfoLog( program, bufsize, out length, infolog)
        return

    [CCode (cname = "wrap_glGetRenderbufferParameteriv")]
    def glGetRenderbufferParameteriv (target : GLenum, pname : GLenum, out parameters : GLint)
        Recorder.recordCall(GlFunc.glGetRenderbufferParameteriv,  target.to_string() + " " + pname.to_string() + " " + parameters.to_string()) 
        GL.raw_glGetRenderbufferParameteriv( target, pname, out parameters)
        return

    [CCode (cname = "wrap_glGetShaderiv")]
    def glGetShaderiv (shader : GLuint, pname : GLenum, out parameters : GLint)
        Recorder.recordCall(GlFunc.glGetShaderiv, shader.to_string() + " " + pname.to_string() + " " + parameters.to_string()) 
        GL.raw_glGetShaderiv( shader, pname, out parameters)
        return

    [CCode (cname = "wrap_glGetShaderInfoLog")]		
    def glGetShaderInfoLog (shader : GLuint, bufsize : GLsizei, out length : GLsizei, infolog : array of GLchar)
        Recorder.recordCall(GlFunc.glGetShaderInfoLog, shader.to_string() + " " + bufsize.to_string() + " " + length.to_string() + " " + TypeSerializer.serializeGLcharArr(infolog))
        GL.raw_glGetShaderInfoLog( shader, bufsize, out length, infolog)
        return

    [CCode (cname = "wrap_glGetShaderPrecisionFormat")]
    def glGetShaderPrecisionFormat (shadertype : GLenum, precisiontype : GLenum, out range : GLint, out precision : GLint)
        Recorder.recordCall(GlFunc.glGetShaderPrecisionFormat, shadertype.to_string() + " " + precisiontype.to_string() + " " + range.to_string() + " " + precision.to_string()) 
        GL.raw_glGetShaderPrecisionFormat( shadertype, precisiontype, out range, out precision )
        return

    [CCode (cname = "wrap_glGetShaderSource")]
    def glGetShaderSource (shader : GLuint, bufsize : GLsizei, out length : GLsizei, source : string)
        Recorder.recordCall(GlFunc.glGetShaderSource, shader.to_string() + " " + bufsize.to_string() + " " + length.to_string() + " " + source.to_string())
        GL.raw_glGetShaderSource( shader, bufsize, out length, source )
        return

    [CCode (cname = "wrap_glGetString")]
    def  glGetString (name : GLenum) : string
        //Recorder.recordCall(GlFunc.glGetString, name.to_string())
        return GL.raw_glGetString(name)

    [CCode (cname = "wrap_glGetTexParameterfv")]
    def glGetTexParameterfv (target : GLenum, pname : GLenum, out parameters : GLfloat)
        Recorder.recordCall(GlFunc.glGetTexParameterfv, target.to_string() + " " + pname.to_string() + " " + parameters.to_string())
        GL.raw_glGetTexParameterfv(target, pname, out parameters )
        return

    [CCode (cname = "wrap_glGetTexParameteriv")]
    def glGetTexParameteriv (target : GLenum, pname : GLenum, out parameters : GLint)
        Recorder.recordCall(GlFunc.glGetTexParameteriv, target.to_string() + " " + pname.to_string() + " " + parameters.to_string()) 
        GL.raw_glGetTexParameteriv( target, pname, out parameters)
        return

    [CCode (cname = "wrap_glGetUniformfv")]
    def glGetUniformfv (program : GLuint, location : GLint, parameters : array of GLfloat)
        Recorder.recordCall(GlFunc.glGetUniformfv, program.to_string() + " " + location.to_string() + " " + TypeSerializer.serializeGLfloatArr(parameters))
        GL.raw_glGetUniformfv( program, location, parameters )
        return

    [CCode (cname = "wrap_glGetUniformiv")]
    def glGetUniformiv (program : GLuint, location : GLint, parameters : array of GLint)
        Recorder.recordCall(GlFunc.glGetUniformiv, program.to_string() + " " + location.to_string() + " " + TypeSerializer.serializeGLintArr(parameters))
        GL.raw_glGetUniformiv( program, location, parameters)
        return

    [CCode (cname = "wrap_glGetUniformLocation")]
    def  glGetUniformLocation (program : GLuint, name : string) : int
        Recorder.recordCall(GlFunc.glGetUniformLocation, program.to_string() + " " + name) 
        return GL.raw_glGetUniformLocation(program, name)

    [CCode (cname = "wrap_glGetVertexAttribfv")]
    def glGetVertexAttribfv (index : GLuint, pname : GLenum, out parameters : GLfloat)
        Recorder.recordCall(GlFunc.glGetVertexAttribfv, index.to_string() + " " + pname.to_string() + " " + parameters.to_string())
        GL.raw_glGetVertexAttribfv( index, pname, out parameters)
        return

    [CCode (cname = "wrap_glGetVertexAttribiv")]
    def glGetVertexAttribiv (index : GLuint, pname : GLenum, out parameters : GLint)
        Recorder.recordCall(GlFunc.glGetVertexAttribiv, index.to_string() + " " + pname.to_string() + " " + parameters.to_string())
        GL.raw_glGetVertexAttribiv( index, pname, out parameters)
        return

    [CCode (cname = "wrap_glGetVertexAttribPointerv")]
    def glGetVertexAttribPointerv (index : GLuint, pname : GLenum, out pointer : GLvoid*)
        Recorder.recordPointerCall(GlFunc.glGetVertexAttribPointerv, index.to_string() + " " + pname.to_string() + " " + ((long)pointer).to_string())
        GL.raw_glGetVertexAttribPointerv( index, pname, out pointer)
        return

    [CCode (cname = "wrap_glHint")]
    def glHint (target : GLenum, mode : GLenum)
        Recorder.recordCall(GlFunc.glHint, target.to_string() + " " + mode.to_string()) 
        GL.raw_glHint(target, mode )
        return

    [CCode (cname = "wrap_glIsBuffer")]
    def glIsBuffer (buffer : GLuint) : GLboolean
        //Recorder.recordCall(GlFunc.glIsBuffer, buffer.to_string())
        return GL.raw_glIsBuffer(buffer)

    [CCode (cname = "wrap_glIsEnabled")]
    def  glIsEnabled (cap : GLenum) : GLboolean
        //Recorder.recordCall(GlFunc.glIsEnabled, cap.to_string())
        return GL.raw_glIsEnabled(cap)

    [CCode (cname = "wrap_glIsFramebuffer")]
    def  glIsFramebuffer (framebuffer : GLuint) : GLboolean
        //Recorder.recordCall(GlFunc.glIsFramebuffer, framebuffer.to_string()) 
        return GL.raw_glIsFramebuffer(framebuffer)

    [CCode (cname = "wrap_glIsProgram")]
    def  glIsProgram (program : GLuint) : GLboolean
        //Recorder.recordCall(GlFunc.glIsProgram, program.to_string())
        return GL.raw_glIsProgram(program)

    [CCode (cname = "wrap_glRenderbuffer")]
    def  glIsRenderbuffer (renderbuffer : GLuint) : GLboolean
        //Recorder.recordCall(GlFunc.glIsRenderbuffer, renderbuffer.to_string())
        return GL.raw_glIsRenderbuffer(renderbuffer)

    [CCode (cname = "wrap_glIsShader")]
    def  glIsShader (shader : GLuint) : GLboolean
        //Recorder.recordCall(GlFunc.glIsShader, shader.to_string())
        return GL.raw_glIsShader(shader)

    [CCode (cname = "wrap_glIsTexture")]
    def  glIsTexture (texture : GLuint) : GLboolean
        //Recorder.recordCall(GlFunc.glIsTexture, texture.to_string())
        return GL.raw_glIsTexture(texture)

    [CCode (cname = "wrap_glLineWidth")]
    def glLineWidth (width : GLfloat)
        Recorder.recordCall(GlFunc.glLineWidth, width.to_string())
        GL.raw_glLineWidth( width )
        return

    [CCode (cname = "wrap_glLinkProgram")]
    def glLinkProgram (program : GLuint)
        Recorder.recordCall(GlFunc.glLinkProgram, program.to_string())
        GL.raw_glLinkProgram(program )
        return

    [CCode (cname = "wrap_glPixelStorei")]
    def glPixelStorei (pname : GLenum, param : GLint)
        Recorder.recordCall(GlFunc.glPixelStorei, pname.to_string() + " " + param.to_string()) 
        GL.raw_glPixelStorei( pname, param)
        return

    [CCode (cname = "wrap_glPolygonOffset")]
    def glPolygonOffset (factor : GLfloat, units : GLfloat )
        Recorder.recordCall(GlFunc.glPolygonOffset, factor.to_string() + " " + units.to_string())
        GL.raw_glPolygonOffset( factor, units)
        return

    [CCode (cname = "wrap_glReadPixels")]
    def glReadPixels (x : GLint, y : GLint, width : GLsizei, height : GLsizei, format : GLenum, type : GLenum, pixels : GLvoid* )
        Recorder.recordPointerCall(GlFunc.glReadPixels, x.to_string() + " " + y.to_string() + " " + width.to_string() + " " + height.to_string() + " " + format.to_string() + " " + type.to_string() + " " + ((long)pixels).to_string())
        GL.raw_glReadPixels( x, y, width, height, format, type, pixels)
        return

    [CCode (cname = "wrap_glReleaseShaderCompiler")]
    def glReleaseShaderCompiler ( )
        Recorder.recordCall(GlFunc.glReleaseShaderCompiler, "") 
        GL.raw_glReleaseShaderCompiler( )
        return

    [CCode (cname = "wrap_glRenderbufferStorage")]
    def glRenderbufferStorage (target : GLenum, internalformat : GLenum, width : GLsizei, height : GLsizei)
        Recorder.recordCall(GlFunc.glRenderbufferStorage, target.to_string() + " " + internalformat.to_string() + " " + width.to_string() + " " + height.to_string())
        GL.raw_glRenderbufferStorage(target, internalformat, width, height )
        return

    [CCode (cname = "wrap_glSampleCoverage")]
    def glSampleCoverage (value : GLclampf, invert : GLboolean )
        Recorder.recordCall(GlFunc.glSampleCoverage, value.to_string() + " " + invert.to_string())
        GL.raw_glSampleCoverage( value, invert)
        return

    [CCode (cname = "wrap_glScissor")]
    def glScissor (x : GLint, y : GLint, width : GLsizei, height : GLsizei)
        Recorder.recordCall(GlFunc.glScissor, x.to_string() + " " + y.to_string() + " " + width.to_string() + " " + height.to_string())
        GL.raw_glScissor( x, y, width, height)
        return

    [CCode (cname = "wrap_glShaderBinary")]
    def glShaderBinary (n : GLsizei, shaders : GLuint*, binaryformat : GLenum , binary : GLvoid*, length : GLsizei)
        Recorder.recordPointerCall(GlFunc.glShaderBinary, n.to_string() + " " + shaders.to_string() + " " + ((long)binaryformat).to_string() + " " + ((long)binary).to_string() + " " + ((long)length).to_string()) 
        GL.raw_glShaderBinary( n, shaders, binaryformat, binary, length)
        return

    [CCode (cname = "wrap_glShaderSource")]
    def glShaderSource (shader : GLuint, count : GLsizei, str : array of GLchar*, length : array of GLint)
        Recorder.recordPointerCall(GlFunc.glShaderSource, shader.to_string() + " " + count.to_string() + " " + TypeSerializer.serializeGLcharptrArr(str) + " " + TypeSerializer.serializeGLintArr(length)) 
        GL.raw_glShaderSource(shader, count, str, length )
        return

    [CCode (cname = "wrap_glStencilFu")]
    def glStencilFunc (func : GLenum, refer : GLint, mask :  GLuint)
        Recorder.recordCall(GlFunc.glStencilFunc, func.to_string() + " " + refer.to_string() + " " + mask.to_string())
        GL.raw_glStencilFunc( func, refer, mask)
        return

    [CCode (cname = "wrap_glStencilFuncSeparate")]
    def glStencilFuncSeparate (face : GLenum, func : GLenum, reference : GLint, mask :  GLuint)
        Recorder.recordCall(GlFunc.glStencilFuncSeparate, face.to_string() + " " + func.to_string() + " " + reference.to_string() + " " + mask.to_string()) 
        GL.raw_glStencilFuncSeparate(face, func, reference, mask )
        return

    [CCode (cname = "wrap_glStencilMask")]
    def glStencilMask (mask :  GLuint)
        Recorder.recordCall(GlFunc.glStencilMask, mask.to_string())
        GL.raw_glStencilMask( mask)
        return

    [CCode (cname = "wrap_glStencilMaskSeparate")]
    def glStencilMaskSeparate (face : GLenum, mask :  GLuint)
        Recorder.recordCall(GlFunc.glStencilMaskSeparate, face.to_string() + " " + mask.to_string()) 
        GL.raw_glStencilMaskSeparate( face, mask)
        return

    [CCode (cname = "wrap_glStencilOp")]
    def glStencilOp (fail : GLenum , zfail : GLenum , zpass : GLenum )
        Recorder.recordCall(GlFunc.glStencilOp, fail.to_string() + " " + zfail.to_string() + " " + zpass.to_string()) 
        GL.raw_glStencilOp(fail, zfail, zpass )
        return

    [CCode (cname = "wrap_glStencilOpSeparate")]
    def glStencilOpSeparate (face : GLenum, fail : GLenum , zfail : GLenum , zpass : GLenum )
        Recorder.recordCall(GlFunc.glStencilOpSeparate, face.to_string() + " " + fail.to_string() + " " + zfail.to_string() + " " + zpass.to_string())
        GL.raw_glStencilOpSeparate(face, fail, zfail, zpass )
        return

    [CCode (cname = "wrap_glTexImage2D")]
    def glTexImage2D (target : GLenum, level : GLint, internalformat : GLint , width : GLsizei, height : GLsizei, border : GLint, format : GLenum, type : GLenum, data : GLvoid*)
        Recorder.recordPointerCall(GlFunc.glTexImage2D, target.to_string() + " " + ((uint)level).to_string() + " " + internalformat.to_string() + " " + width.to_string() + " " + height.to_string() + " " + border.to_string() + " " + format.to_string() + " " + type.to_string() + " " + ((long)data).to_string()) 
        GL.raw_glTexImage2D( target, level, internalformat, width, height, border, format, type, data)
        return

    [CCode (cname = "wrap_glTexParameterf")]
    def glTexParameterf (target : GLenum, pname : GLenum, param : GLfloat )
        Recorder.recordCall(GlFunc.glTexParameterf, target.to_string() + " " + pname.to_string() + " " + param.to_string()) 
        GL.raw_glTexParameterf( target, pname, param)
        return

    [CCode (cname = "wrap_glTexParameterfv")]
    def glTexParameterfv (target : GLenum, pname : GLenum, parameters : array of GLfloat)
        Recorder.recordCall(GlFunc.glTexParameterfv, target.to_string() + " " + pname.to_string() + " " + TypeSerializer.serializeGLfloatArr(parameters))
        GL.raw_glTexParameterfv( target, pname, parameters)
        return

    [CCode (cname = "wrap_glTexParameteri")]
    def glTexParameteri (target : GLenum, pname : GLenum, param : GLint )
        Recorder.recordCall(GlFunc.glTexParameteri, target.to_string() + " " + pname.to_string() + " " + param.to_string())
        GL.raw_glTexParameteri( target, pname, param)
        return

    [CCode (cname = "wrap_glTexParameteriv")]
    def glTexParameteriv (target : GLenum, pname : GLenum, parameters : array of GLint)
        Recorder.recordCall(GlFunc.glTexParameteriv, target.to_string() + " " + pname.to_string() + " " + TypeSerializer.serializeGLintArr(parameters))
        GL.raw_glTexParameteriv(target, pname, parameters )
        return

    [CCode (cname = "wrap_glTexSubImage2D")]
    def glTexSubImage2D (target : GLenum, level : GLint, xoffset : GLint, yoffset : GLint, width : GLsizei, height : GLsizei, format : GLenum, type : GLenum, data : GLvoid*)
        Recorder.recordPointerCall(GlFunc.glTexSubImage2D,  target.to_string() + " " + level.to_string() + " " + xoffset.to_string() + " "+ yoffset.to_string() + " " + width.to_string() + " " + height.to_string() + " " + format.to_string() + " " + type.to_string() + " " + ((long)data).to_string()) 
        GL.raw_glTexSubImage2D( target, level, xoffset, yoffset, width, height, format, type, data)
        return

    [CCode (cname = "wrap_glUniform1f")]
    def glUniform1f (location : GLint, x : GLfloat)
        Recorder.recordCall(GlFunc.glUniform1f, location.to_string() + " " + x.to_string())
        GL.raw_glUniform1f( location, x)
        return

    [CCode (cname = "wrap_glUniform1fv")]
    def glUniform1fv (location : GLint, count : GLsizei, v : GLfloat*)
        Recorder.recordPointerCall(GlFunc.glUniform1fv, location.to_string() + " " + count.to_string() + " " + ((long)v).to_string()) 
        GL.raw_glUniform1fv(location, count, v )
        return

    [CCode (cname = "wrap_glUniform1i")]
    def glUniform1i (location : GLint, x : GLint)
        Recorder.recordCall(GlFunc.glUniform1i, location.to_string() + " " + x.to_string())
        GL.raw_glUniform1i( location, x)
        return

    [CCode (cname = "wrap_glUniform1iv")]
    def glUniform1iv (location : GLint, count : GLsizei, v : GLint*)
        Recorder.recordPointerCall(GlFunc.glUniform1iv,  location.to_string() + " " + count.to_string() + " " + ((long)v).to_string()) 
        GL.raw_glUniform1iv(location, count, v )
        return

    [CCode (cname = "wrap_glUniform2f")]
    def glUniform2f (location : GLint, x : GLfloat, y : GLfloat)
        Recorder.recordCall(GlFunc.glUniform2f, location.to_string() + " " + x.to_string() + " " + y.to_string())
        GL.raw_glUniform2f( location, x, y)
        return

    [CCode (cname = "wrap_glUniform2fv")]
    def glUniform2fv (location : GLint, count : GLsizei, v : GLfloat*)
        Recorder.recordPointerCall(GlFunc.glUniform2fv, location.to_string() + " " + count.to_string() + " " + ((long)v).to_string())
        GL.raw_glUniform2fv( location, count, v )
        return

    [CCode (cname = "wrap_glUniform2i")]
    def glUniform2i (location : GLint, x : GLint, y : GLint)
        Recorder.recordCall(GlFunc.glUniform2i, location.to_string() + " " + x.to_string() + " " + y.to_string())
        GL.raw_glUniform2i( location, x, y)
        return

    [CCode (cname = "wrap_glUniform2iv")]
    def glUniform2iv (location : GLint, count : GLsizei, v : GLint*)
        Recorder.recordPointerCall(GlFunc.glUniform2iv, location.to_string() + " " + count.to_string() + " " + ((long)v).to_string())
        GL.raw_glUniform2iv( location, count, v)
        return

    [CCode (cname = "wrap_glUniform3f")]
    def glUniform3f (location : GLint, x : GLfloat, y : GLfloat, z : GLfloat)
        Recorder.recordCall(GlFunc.glUniform3f, location.to_string() + " " + x.to_string() + " " + y.to_string() + " " + z.to_string())
        GL.raw_glUniform3f( location, x, y, z)
        return

    [CCode (cname = "wrap_glUniform3fv")]
    def glUniform3fv (location : GLint, count : GLsizei, v : GLfloat*)
        Recorder.recordPointerCall(GlFunc.glUniform3fv, location.to_string() + " " + count.to_string() + " " + ((long)v).to_string())
        GL.raw_glUniform3fv( location, count, v)
        return

    [CCode (cname = "wrap_glUniform3i")]
    def glUniform3i (location : GLint, x : GLint, y : GLint, z : GLint)
        Recorder.recordCall(GlFunc.glUniform3i, x.to_string() + " " + y.to_string() + " " + z.to_string())
        GL.raw_glUniform3i( location, x, y, z)
        return

    [CCode (cname = "wrap_glUniform3iv")]
    def glUniform3iv (location : GLint, count : GLsizei, v : GLint*)
        Recorder.recordPointerCall(GlFunc.glUniform3iv, location.to_string() + " " + count.to_string() + " " + ((long)v).to_string())
        GL.raw_glUniform3iv( location, count, v)
        return

    [CCode (cname = "wrap_glUniform4f")]
    def glUniform4f (location : GLint, x : GLfloat, y : GLfloat, z : GLfloat, w : GLfloat)
        Recorder.recordCall(GlFunc.glUniform4f, x.to_string() + " " + y.to_string() + " " + z.to_string() + w.to_string())
        GL.raw_glUniform4f( location, x, y, z, w)
        return

    [CCode (cname = "wrap_glUniform4fv")]
    def glUniform4fv (location : GLint, count : GLsizei, v : GLfloat*)
        Recorder.recordPointerCall(GlFunc.glUniform4fv, location.to_string() + " " + count.to_string() + " " + ((long)v).to_string())
        GL.raw_glUniform4fv( location, count, v)
        return

    [CCode (cname = "wrap_glUniform4i")]
    def glUniform4i (location : GLint, x : GLint, y : GLint, z : GLint, w : GLint)
        Recorder.recordCall(GlFunc.glUniform4i,  x.to_string() + " " + y.to_string() + " " + z.to_string() + " " + w.to_string())
        GL.raw_glUniform4i( location, x, y, z, w)
        return

    [CCode (cname = "wrap_glUniform4iv")]
    def glUniform4iv (location : GLint, count : GLsizei, v : GLint*)
        Recorder.recordPointerCall(GlFunc.glUniform4iv, location.to_string() + " " + count.to_string() + " " + ((long)v).to_string())
        GL.raw_glUniform4iv( location, count, v)
        return

    [CCode (cname = "wrap_glUniformMatrix2fv")]
    def glUniformMatrix2fv (location : GLint, count : GLsizei, transpose : GLboolean, v : GLfloat*)
        Recorder.recordPointerCall(GlFunc.glUniformMatrix2fv, location.to_string() + " " + count.to_string() + " " + transpose.to_string() + " " + ((long)v).to_string())
        GL.raw_glUniformMatrix2fv( location, count, transpose, v)
        return

    [CCode (cname = "wrap_glUniformMatrix3fv")]
    def glUniformMatrix3fv (location : GLint, count : GLsizei, transpose : GLboolean, v : GLfloat*)
        Recorder.recordPointerCall(GlFunc.glUniformMatrix3fv, location.to_string() + " " + count.to_string() + " " + transpose.to_string() + " " + ((long)v).to_string())
        GL.raw_glUniformMatrix3fv( location, count, transpose, v)
        return

    [CCode (cname = "wrap_glUniformMatrix4fv")]
    def glUniformMatrix4fv (location : GLint, count : GLsizei, transpose : GLboolean, v : GLfloat*)
        Recorder.recordPointerCall(GlFunc.glUniformMatrix4fv,  location.to_string() + " " + count.to_string() + " " + transpose.to_string() + " " + ((long)v).to_string())
        GL.raw_glUniformMatrix4fv( location, count, transpose, v)
        return

    [CCode (cname = "wrap_glUseProgram")]
    def glUseProgram (program : GLuint)
        Recorder.recordCall(GlFunc.glUseProgram, program.to_string())
        GL.raw_glUseProgram( program)
        return

    [CCode (cname = "wrap_glValidateProgram")]
    def glValidateProgram (program : GLuint)
        Recorder.recordCall(GlFunc.glValidateProgram, program.to_string())
        GL.raw_glValidateProgram( program)
        return

    [CCode (cname = "wrap_glVertexAttrib1f")]
    def glVertexAttrib1f (indx : GLuint, x : GLfloat)
        Recorder.recordCall(GlFunc.glVertexAttrib1f, indx.to_string() + " " + x.to_string())
        GL.raw_glVertexAttrib1f( indx, x)
        return

    [CCode (cname = "wrap_glVertexAttrib1fv")]
    def glVertexAttrib1fv (indx : GLuint, values : GLfloat*)
        Recorder.recordPointerCall(GlFunc.glVertexAttrib1fv, indx.to_string() + " " + values.to_string())
        GL.raw_glVertexAttrib1fv( indx ,values)
        return

    [CCode (cname = "wrap_glVertexAttrib2f")]
    def glVertexAttrib2f (indx : GLuint, x : GLfloat, y : GLfloat)
        Recorder.recordCall(GlFunc.glVertexAttrib2f, indx.to_string() + " " + x.to_string() + " " + y.to_string())
        GL.raw_glVertexAttrib2f(indx, x, y )
        return

    [CCode (cname = "wrap_glVertexAttrib2fv")]
    def glVertexAttrib2fv (indx : GLuint, values : GLfloat*)
        Recorder.recordPointerCall(GlFunc.glVertexAttrib2fv, indx.to_string() + " " + values.to_string())
        GL.raw_glVertexAttrib2fv(indx, values )
        return

    [CCode (cname = "wrap_glVertexAttrib3f")]
    def glVertexAttrib3f (indx : GLuint, x : GLfloat, y : GLfloat, z : GLfloat)
        Recorder.recordCall(GlFunc.glVertexAttrib3f, indx.to_string() + " " +  x.to_string() + " " + y.to_string() + " " + z.to_string()) 
        GL.raw_glVertexAttrib3f( indx, x, y, z)
        return

    [CCode (cname = "wrap_glVertexAttrib3fv")]
    def glVertexAttrib3fv (indx : GLuint, values : GLfloat*)
        Recorder.recordPointerCall(GlFunc.glVertexAttrib3fv, indx.to_string() + " " + values.to_string())
        GL.raw_glVertexAttrib3fv( indx, values)
        return

    [CCode (cname = "wrap_glVertexAttrib4f")]
    def glVertexAttrib4f (indx : GLuint, x : GLfloat, y : GLfloat, z : GLfloat, w : GLfloat)
        Recorder.recordCall(GlFunc.glVertexAttrib4f, indx.to_string() + " " +  x.to_string() + " " + y.to_string() + " " + z.to_string() + " " + w.to_string()) 
        GL.raw_glVertexAttrib4f( indx, x, y, z, w)
        return

    [CCode (cname = "wrap_glVertexAttrib4fv")]
    def glVertexAttrib4fv (indx : GLuint, values : GLfloat*)
        Recorder.recordPointerCall(GlFunc.glVertexAttrib4fv, indx.to_string() + " " + values.to_string())
        GL.raw_glVertexAttrib4fv( indx, values)
        return

    [CCode (cname = "wrap_glVertexAttribPointer")]
    def glVertexAttribPointer (indx : GLuint, size : GLint, type : GLenum, normalized : GLboolean , stride : GLsizei , ptr : GLvoid*)
        Recorder.recordCall(GlFunc.glVertexAttribPointer, indx.to_string() + " " + size.to_string() + " " + type.to_string() + " " + normalized.to_string() + " " + stride.to_string() + " " +((long) ptr).to_string())
        GL.raw_glVertexAttribPointer(indx, size, type, normalized, stride, ptr )
        return

    [CCode (cname = "wrap_glViewport")]
    def glViewport (x : GLint, y : GLint, width : GLsizei, height : GLsizei)
        Recorder.recordCall(GlFunc.glViewport, x.to_string() + " " + y.to_string() + " " + width.to_string() + " " + height.to_string())
        GL.raw_glViewport( x, y, width, height)
        return

