/*
 *  libsoy - soy.recording.Recorder
 *  Copyright (C) 2006-2015 Copyleft Games Group
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, see http://www.gnu.org/licenses
 *
 */

[indent=4]
uses GL

class soy.recording.Recorder : Object
    funcNameBuf : static list of GlFunc 
    paramBuf : static list of string

    bufferFlushThresh : static int = 5000
    outputFile : static FileStream

    const recordingFile : string = "game.rec"
    recorderState : static RecorderModuleState = RecorderModuleState.doNothing
    playbackStarted : static bool = false

    //static initializers are broken in genie so we have to implement it ourselves
    def static initialize()
        print "gl recording buffers initialized"
        funcNameBuf = new list of GlFunc
        paramBuf = new list of string
        var file = File.new_for_path(recordingFile)
        outputFile = FileStream.open(recordingFile,"w")


    def static clearRecordingBuffers()
        funcNameBuf.clear()
        paramBuf.clear()

    def static writeBuffers()
        //we're only doing it like this until add json libs
        i : int
        for i = 0 to (funcNameBuf.size-1)
            var funcStr = ((int)funcNameBuf[i]).to_string() + " " + paramBuf[i] + "\r\n"
            outputFile.puts(funcStr)

        outputFile.flush();
        clearRecordingBuffers()


    def static recordCall (funcName : GlFunc, parameters : string)
        //var err = GL.raw_glGetError()
        //if err != 0
            //print "glerror: "+ err.to_string() + " " + funcName.to_string()
        if !playbackStarted
            Playback.startPlayback("game.rec")
            playbackStarted = true
        if recorderState == RecorderModuleState.playbackMedia
            return
        if recorderState == RecorderModuleState.recordMedia
            if funcNameBuf is null
                initialize()
            funcNameBuf.add (funcName)
            paramBuf.add (parameters)

            //check if buffer is full
            //write to file if buffer is full
            if funcNameBuf.size > bufferFlushThresh
                writeBuffers()
        return

    def static recordPointerCall ( funcName : GlFunc, parameters : string)
        var expandedParams = PointerSerializer.serializeDataFunction(funcName, parameters)
        recordCall(funcName, expandedParams)
