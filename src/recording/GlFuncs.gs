/*
 *  libsoy - soy.recording.Recorder
 *  Copyright (C) 2006-2015 Copyleft Games Group
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, see http://www.gnu.org/licenses
 *
 */

[indent=4]

namespace soy.recording
    enum GlFunc
        glActiveTexture
        glAttachShader
        glBindAttribLocation
        glBindBuffer
        glBindFramebuffer
        glBindRenderbuffer
        glBindTexture
        glBlendColor
        glBlendEquation
        glBlendEquationSeparate
        glBlendFunc
        glBlendFuncSeparate
        glBufferData
        glBufferSubData
        glCheckFramebufferStatus
        glClear
        glClearColor
        glClearDepthf
        glClearStencil
        glColorMask
        glCompileShader
        glCompressedTexImage2D
        glCompressedTexSubImage2D
        glCopyTexImage2D
        glCopyTexSubImage2D
        glCreateProgram
        glCreateShader
        glCullFace
        glDeleteBuffers
        glDeleteFramebuffers
        glDeleteProgram
        glDeleteRenderbuffers
        glDeleteShader
        glDeleteTextures
        glDepthFunc
        glDepthMask
        glDepthRangef
        glDetachShader
        glDisable
        glDisableVertexAttribArray
        glDrawArrays
        glDrawElements
        glEnable
        glEnableVertexAttribArray
        glFinish
        glFlush
        glFramebufferRenderbuffer
        glFramebufferTexture2D
        glFrontFace
        glGenBuffers
        glGenerateMipmap
        glGenFramebuffers
        glGenRenderbuffers
        glGenTextures
        glGetActiveAttrib
        glGetActiveUniform
        glGetAttachedShaders
        glGetAttribLocation
        glGetBooleanv
        glGetBufferParameteriv
        glGetError
        glGetFloatv
        glGetFramebufferAttachmentParameteriv
        glGetIntegerv
        glGetProgramiv
        glGetProgramInfoLog
        glGetRenderbufferParameteriv
        glGetShaderiv
        glGetShaderInfoLog
        glGetShaderPrecisionFormat
        glGetShaderSource
        glGetString
        glGetTexParameterfv
        glGetTexParameteriv
        glGetUniformfv
        glGetUniformiv
        glGetUniformLocation
        glGetVertexAttribfv
        glGetVertexAttribiv
        glGetVertexAttribPointerv
        glHint
        glIsBuffer
        glIsEnabled
        glIsFramebuffer
        glIsProgram
        glIsRenderbuffer
        glIsShader
        glIsTexture
        glLineWidth
        glLinkProgram
        glPixelStorei
        glPolygonOffset
        glReadPixels
        glReleaseShaderCompiler
        glRenderbufferStorage
        glSampleCoverage
        glScissor
        glShaderBinary
        glShaderSource
        glStencilFunc
        glStencilFuncSeparate
        glStencilMask
        glStencilMaskSeparate
        glStencilOp
        glStencilOpSeparate
        glTexImage2D
        glTexParameterf
        glTexParameterfv
        glTexParameteri
        glTexParameteriv
        glTexSubImage2D
        glUniform1f
        glUniform1fv
        glUniform1i
        glUniform1iv
        glUniform2f
        glUniform2fv
        glUniform2i
        glUniform2iv
        glUniform3f
        glUniform3fv
        glUniform3i
        glUniform3iv
        glUniform4f
        glUniform4fv
        glUniform4i
        glUniform4iv
        glUniformMatrix2fv
        glUniformMatrix3fv
        glUniformMatrix4fv
        glUseProgram
        glValidateProgram
        glVertexAttrib1f
        glVertexAttrib1fv
        glVertexAttrib2f
        glVertexAttrib2fv
        glVertexAttrib3f
        glVertexAttrib3fv
        glVertexAttrib4f
        glVertexAttrib4fv
        glVertexAttribPointer
        glViewport
        nonglINJECTDRAWWAIT
        eglMakeCurrent
        eglSwapBuffers
