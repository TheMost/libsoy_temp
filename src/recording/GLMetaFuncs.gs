/*
 *  libsoy - soy.recording.GLMetaFuncs.gs
 *  Copyright (C) 2006-2015 Copyleft Games Group
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, see http://www.gnu.org/licenses
 *
 */

[indent=4]
uses GL

class soy.recording.GLMetaFuncs : Object
    def static getGLDatatypeSize(identifier : int) : int
        case identifier
            when (int)GL_BYTE
                return 1 
            when (int)GL_UNSIGNED_BYTE
                return 1
            when (int)GL_SHORT
                return 2
            when (int)GL_UNSIGNED_SHORT
                return 2
            when (int)GL_FLOAT
                return 4
            when (int)GL_FIXED
                return 4
            when (int)GL_UNSIGNED_SHORT_4_4_4_4
                return 2
            when (int)GL_UNSIGNED_SHORT_5_5_5_1
                return 2
            when (int)GL_UNSIGNED_SHORT_5_6_5
                return 2
            default
                assert(false)
        return -1

    def static decodeGLDataType(identifier : int) : string
        case identifier
            when (int)GL_BYTE
                return "GL_BYTE"
            when (int)GL_UNSIGNED_BYTE
                return "GL_UNSIGNED_BYTE"
            when (int)GL_SHORT
                return "GL_SHORT"
            when (int)GL_UNSIGNED_SHORT
                return "GL_UNSIGNED_SHORT"
            when (int)GL_FLOAT
                return "GL_FLOAT"
            when (int)GL_FIXED
                return "GL_FIXED"
            when (int)GL_UNSIGNED_SHORT_4_4_4_4
                return "GL_UNSIGNED_SHORT_4_4_4_4"
            when (int)GL_UNSIGNED_SHORT_5_5_5_1
                return "GL_UNSIGNED_SHORT_5_5_5_5"
            when (int)GL_UNSIGNED_SHORT_5_6_5
                return "GL_UNSIGNED_SHORT_5_6_5"
            default
                assert(false)
        return ""

