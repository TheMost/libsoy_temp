/*
 *  libsoy - soy.bodies.Grass
 *  Copyright (C) 2006-2015 Copyleft Games Group
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, see http://www.gnu.org/licenses
 *
 */

[indent=4]
uses
    GL
    GLib.Math
    GLib.Random
    ode
    soy.atoms


class soy.bodies.Grass : soy.bodies.LandscapeBody
    _ebo : GLuint   // Element Buffer Object
    _vbo : GLuint   // Vertex Buffer Object
    _updated : bool // Buffers need updating
    _elenum : int
    _vertices : array of GLfloat
    _density : int
    _time : float = 0


    construct (position : soy.atoms.Position?, size : soy.atoms.Size?,
               material : soy.materials.Material?, density : int = 500)
        super(position, size, material)

        // Setup for first render pass
        _ebo = 0
        _updated = true
        _density = density

        // Set default material
        if material is null
            if default_material is null
                default_material = new soy.materials.Material()
            _material = default_material

        // Use the provided material
        else
            _material = material



    ////////////////////////////////////////////////////////////////////////
    // Properties

    //
    // Material Property
    _material : soy.materials.Material
    prop new material : soy.materials.Material?
        get
            if _material is default_material
                return null
            return _material
        set
            mutex.lock()
            // Use default material
            if value is null
                if default_material is null
                    default_material = new soy.materials.Material()
                _material = default_material

            // Use the provided material
            else
                _material = value
            mutex.unlock()


    def override add_extra()
        if scene isa soy.scenes.Landscape
            return
            // var landscape = (soy.scenes.Landscape)scene
            // self.position.x +=scene.position.x
            // self.position.y +=scene.position.y
            // self.position.z +=scene.position.z

    def _update_grass()
        // on first pass
        if _ebo == 0
            buffers : array of GLuint = {0,0}
            glGenBuffers(buffers)
            _ebo = buffers[0]
            _vbo = buffers[1]

        var blade_elenum = 5
        var blade_vernum = 7

        blade_elements : array of GLushort = new array of GLushort[blade_elenum * 3]
        blade_coord : array of GLfloat = new array of GLfloat[blade_vernum * 3]
        blade_texcoord : array of GLfloat = new array of GLfloat[blade_vernum * 2]

        blade_elements = {0, 1, 2, 2, 3, 0, 2, 4, 3, 3 ,4 ,5 ,6 ,4 ,5}
        blade_coord = {
        0.0f, 0.0f, 0.0f,
        0.25f, 0.0f, 0.0f,
        0.25f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.305f, 2.0f, 0.0f,
        0.1f, 2.0f, 0.0f,
        0.35f, 3.0f, 0.0f
        }
        blade_texcoord = {
        0.0f, 0.0f,
        1.0f, 0.0f,
        1.0f, 0.3f,
        0.0f, 0.3f,
        1.0f, 0.6f,
        0.0f, 0.6f,
        1.0f, 1.0f
        }

        // number of elements (primitives)
        var elenum = blade_elenum * _density
        // number of vertexis
        var vernum = blade_vernum * _density

        elements : array of GLushort = new array of GLushort[elenum*3]
        coord : array of GLfloat = new array of GLfloat[vernum*3]
        normal : array of GLfloat = new array of GLfloat[vernum*3]
        texcoord : array of GLfloat = new array of GLfloat[vernum*2]
        tangent : array of GLfloat = new array of GLfloat[vernum*3]
        var ie = 0
        var iv = 0
        var in = 0
        rot : float
        for var i = 0 to (_density - 1)
            rot = (float)double_range(0.0, 400.0)
            for var j = 0 to (blade_elenum * 3 - 1)
                elements[ie++] = blade_elements[j] + (blade_vernum * i)
            for var j = 0 to (blade_vernum - 1)
                coord[iv++] = blade_coord[j * 3] * sinf(rot)
                coord[iv++] = blade_coord[j * 3 + 1] // TODO: adjust to terrain height
                coord[iv++] = blade_coord[j * 3] * cosf(rot)
            for var j = 0 to (blade_vernum * 2 - 1)
                texcoord[in++] = blade_texcoord[j]

        _vertices = packArrays(coord, normal, texcoord, tangent)
        iv=0
        var offset_y = 0.0f
        for var i = 0 to (_density - 1)
            var offset_x = (float)double_range(-self.size.width/2, self.size.width/2)
            var offset_z = (float)double_range(-self.size.depth/2, self.size.depth/2)
            if scene isa soy.scenes.Landscape
                var landscape = (soy.scenes.Landscape)scene
                if landscape.heightmap isa soy.textures.Heightmap
                    var heightmap = (soy.textures.Heightmap)landscape.heightmap
                    var x = ((offset_x - self.position.x + (landscape.size.width / 2)) / landscape.size.width * (heightmap.size.width - 1))
                    var z = ((offset_z - self.position.z + (landscape.size.depth / 2)) / landscape.size.depth * (heightmap.size.height - 1))
                    var grid_x = (int)x
                    var grid_z = (int)z
                    var weight_x = x - grid_x
                    var weight_z = z - grid_z
                    // out of terrain
                    if  (offset_x + self.position.x > landscape.size.width/2.0f or offset_x + self.position.x < -landscape.size.width/2.0f or offset_z + self.position.z > landscape.size.depth/2.0f or offset_z + self.position.z < -landscape.size.depth/2.0f)
                        offset_y = 0.0f
                    else
                        // todo: solve this offset using the planae equations
                        offset_y = (((heightmap[grid_z * (int)heightmap.size.width + grid_x] * (1.0f - weight_z )
                                     + heightmap[(grid_z + 1) * (int)heightmap.size.width + grid_x] * weight_z) * ( 1.0f - weight_x)
                                     + (heightmap[grid_z * (int)heightmap.size.width + grid_x + 1] * (1.0f - weight_z)
                                     + heightmap[(grid_z + 1) * (int)heightmap.size.width + grid_x + 1] * weight_z) * weight_x
                                     ) / 255.0f ) * landscape.size.height

            for var j = 0 to (blade_vernum - 1)
                _vertices[iv] += offset_x
                _vertices[iv+1] += offset_y
                _vertices[iv+2] += offset_z
    
                iv+=11


        // bind elements
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ebo)
        if self._material.draw_mode is GL_LINES
            elements = calculateEdges(coord, elements)
            elenum = elements.length/2
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, (GLsizei) (2 * elenum * sizeof(
                     GLushort)), elements, GL_STATIC_DRAW)
        else
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, (GLsizei) (3 * elenum * sizeof(
                     GLushort)), elements, GL_STATIC_DRAW)

        // bind vertices
        glBindBuffer(GL_ARRAY_BUFFER, _vbo)
        glBufferData(GL_ARRAY_BUFFER, (GLsizei) (11 * vernum * sizeof(GLfloat)),
                    _vertices, GL_DYNAMIC_DRAW)

        _elenum = elenum
        // Reset updated flag
        _updated = false

    def override render ( alpha_stage : bool, view : array of GLfloat,
                          projection : array of GLfloat, lights : array of
                          soy.bodies.Light, ambient : array of GLfloat,
                          fog_color: array of GLfloat )
        if alpha_stage is not self._material.translucent
            return

        // Lock so body cant be changed during render
        mutex.lock()

        // get model matrix
        model : array of GLfloat = self.model_matrix()

        // modelview matrix
        model_view : array of GLfloat[16] = self.calculate_model_view(model, view)

        if _updated
            _update_grass()
        // Re-bind buffers when not updating
        else
            var blade_vernum = 7
            var vertices = new array of GLfloat[(blade_vernum * _density * 11)]
            deltaX : array of GLfloat = {0.0f, 0.0f, 0.0f}
            deltaY : array of GLfloat = {0.0f, 0.0f, 0.0f}
            deltaZ : array of GLfloat = {0.0f, 0.0f, 0.0f}
            weights : array of GLfloat = {0.1f, 0.2f, 0.3f}
            _time += 0.08f
            // TODO: change to a more realistic movement
            var i=0
            var direction = new soy.atoms.Vector(0.0f, 0.0f, 0.0f)
            var wind_power = 0.0f
            for var field in scene.fields.values.to_array()
                if field isa soy.fields.Wind
                    var wind = (soy.fields.Wind)field
                    wind_power += wind.density
                    direction.add(wind.wind)
            if (direction.x != 0 or direction.y != 0 or direction.z != 0)
                direction.normalize();

            for var blade=0 to (_density - 1)
                for var level = 0 to 2
                    deltaX[level] = (1.3f+sinf(_time * wind_power + (direction.x * _vertices[i*11] / 3))) * weights[level] * direction.x 
                    deltaY[level] = -fabsf(deltaX[level]) / 2
                    deltaZ[level] = (1.3f+sinf(_time * wind_power + (direction.x * _vertices[i*11] / 3))) * weights[level] * direction.z 
                for var j=0 to (blade_vernum - 1)
                    vertices[i*11] = _vertices[i*11]
                    vertices[i*11+1] = _vertices[i*11+1]
                    vertices[i*11+2] = _vertices[i*11+2]
                    vertices[i*11+3] = _vertices[i*11+3]
                    vertices[i*11+4] = _vertices[i*11+4]
                    vertices[i*11+5] = _vertices[i*11+5]
                    vertices[i*11+6] = _vertices[i*11+6]
                    vertices[i*11+7] = _vertices[i*11+7]
                    vertices[i*11+8] = _vertices[i*11+8]
                    vertices[i*11+9] = _vertices[i*11+9]
                    vertices[i*11+10] = _vertices[i*11+10]
                    if j > 1
                        vertices[i*11+0] += deltaX[0]
                        vertices[i*11+1] += deltaY[0]
                        vertices[i*11+2] += deltaZ[0]
                    if j > 3
                        vertices[i*11+0] += deltaX[1]
                        vertices[i*11+1] += deltaY[1]
                        vertices[i*11+2] += deltaZ[1]
                    if j > 5
                        vertices[i*11+0] += deltaX[2]
                        vertices[i*11+1] += deltaY[2]
                        vertices[i*11+2] += deltaZ[2]
                    i++

            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ebo)
            glBindBuffer(GL_ARRAY_BUFFER, _vbo)
            glBufferData(GL_ARRAY_BUFFER, (GLsizei) (11 * blade_vernum * _density * sizeof(GLfloat)),
                    vertices, GL_DYNAMIC_DRAW)




        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, (GLsizei)
                              (sizeof(GLfloat) * 11), null)
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, (GLsizei)
                              (sizeof(GLfloat) * 11), (GLvoid*)
                              (sizeof(GLfloat) * 3))
        glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, (GLsizei)
                              (sizeof(GLfloat) * 11), (GLvoid*)
                              (sizeof(GLfloat) * 6))
        glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, (GLsizei)
                              (sizeof(GLfloat) * 11), (GLvoid*)
                              (sizeof(GLfloat) * 8))

        glDisable(GL_CULL_FACE);

        glEnableVertexAttribArray(0)
        glEnableVertexAttribArray(1)
        glEnableVertexAttribArray(2)
        glEnableVertexAttribArray(3)

        var numVert = 3
        if self._material.draw_mode is GL_LINES
            numVert = 2

        i : int = 0
        while self._material.enable(i, model_view, view, projection, lights,
                                    ambient, fog_color)
            glDrawElements(self._material.draw_mode, (GLsizei) numVert*_elenum,
                           GL_UNSIGNED_SHORT, (GLvoid*) 0)
            i++

        glDisableVertexAttribArray(0)
        glDisableVertexAttribArray(1)
        glDisableVertexAttribArray(2)
        glDisableVertexAttribArray(3)

        glEnable(GL_CULL_FACE);

        self._material.disable()

        // Rendering done, unlock
        mutex.unlock()


