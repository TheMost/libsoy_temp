/*
 *  libsoy - soy.bodies.LandscapeBody
 *  Copyright (C) 2006-2015 Copyleft Games Group
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, see http://www.gnu.org/licenses
 *
 */

[indent=4]
uses
    GL
    GLib.Math
    ode
    soy.atoms


class soy.bodies.LandscapeBody : soy.bodies.Body
    _ebo : GLuint   // Element Buffer Object
    _updated : bool // Buffers need updating
    _size : soy.atoms.Size?
    default_size : soy.atoms.Size?

    construct (position : soy.atoms.Position?, size : soy.atoms.Size?,
               material : soy.materials.Material?)
        super(position, size, 0.0f)
        // Setup for first render pass
        _ebo = 0
        _updated = true


        // Set default material
        if material is null
            if default_material is null
                default_material = new soy.materials.Material()
            _material = default_material
        // Use the provided material
        else
            _material = material

        if size is null
            if default_size is null
                default_size = new soy.atoms.Size()
            _size = default_size
        else
            _size = size

        geom.Disable()



    ////////////////////////////////////////////////////////////////////////
    // Properties

    //
    // Material Property
    _material : soy.materials.Material
    prop material : soy.materials.Material?
        get
            if _material is default_material
                return null
            return _material
        set
            mutex.lock()
            // Use default material
            if value is null
                if default_material is null
                    default_material = new soy.materials.Material()
                _material = default_material

            // Use the provided material
            else
                _material = value
            mutex.unlock()

    //
    // Size Property
    prop size : soy.atoms.Size?
        get
            if _size is default_size
                return null
            return _size
        set
            mutex.lock()
            if value is null
                if default_size is null
                    default_size = new soy.atoms.Size()
                _size = default_size
            else
                _size = value
            mutex.unlock()

   
    def override add_extra()
        if scene isa soy.scenes.Landscape
            return
            // var landscape = (soy.scenes.Landscape)scene

            // self.position.x +=scene.position.x
            // self.position.y +=scene.position.y
            // self.position.z +=scene.position.z


    def override render ( alpha_stage : bool, view : array of GLfloat,
                          projection : array of GLfloat, lights : array of
                          soy.bodies.Light, ambient : array of GLfloat,
                          fog_color: array of GLfloat )
        if alpha_stage is not self._material.translucent
            return

        // Lock so body cant be changed during render
        mutex.lock()


        // Rendering done, unlock
        mutex.unlock()


