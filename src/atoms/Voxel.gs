/*
 *  libsoy - soy.atoms.Voxel
 *  Copyright (C) 2006-2015 Copyleft Games Group
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, see http://www.gnu.org/licenses
 *
 */

[indent=4]

class soy.atoms.Voxel : Object
    event on_set (voxel : soy.atoms.Voxel)

    construct (color : soy.atoms.Color)
        // Store color
        self._cr = (uchar) color.red
        self._cg = (uchar) color.green
        self._cb = (uchar) color.blue
        self._color_obj = color
        color.on_set.connect(self._color_set)
        color.weak_ref(self._color_weak)


    ////////////////////////////////////////////////////////////////////////
    // Properties

    //
    // color Property
    _cr : uchar
    _cg : uchar
    _cb : uchar
    _color_obj : weak soy.atoms.Color?

    def _color_set(color : soy.atoms.Color)
        self._cr = (uchar) color.red
        self._cg = (uchar) color.green
        self._cb = (uchar) color.blue
        self.on_set(self)

    def _color_weak(position : Object)
        self._color_obj = null

    prop color : soy.atoms.Color
        owned get
            value : soy.atoms.Color? = self._color_obj
            if value is null
                value = new soy.atoms.Color((uchar) self._cr,
                                               (uchar) self._cg,
                                               (uchar) self._cb)
                value.on_set.connect(self._color_set)
                value.weak_ref(self._color_weak)
                self._color_obj = value
            return value
        set
            self._color_set(value)
            if _color_obj != null
                _color_obj.on_set.disconnect(self._color_set)
                _color_obj.weak_unref(self._color_weak)
            _color_obj = value
            value.on_set.connect(self._color_set)
            value.weak_ref(self._color_weak)

    def static cmp_eq (left : Object, right : Object) : bool
        if not (left isa soy.atoms.Voxel) or not (right isa soy.atoms.Voxel)
            return false

        _col : bool = soy.atoms.Color.cmp(((soy.atoms.Voxel) left).color,
                                          ((soy.atoms.Voxel) right).color,
					  Comparison.EQ)

        return _col


    def static cmp_ne (left : Object, right : Object) : bool
        return not cmp_eq(left, right)
