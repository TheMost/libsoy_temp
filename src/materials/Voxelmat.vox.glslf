/*
 *  libsoy - soy.materials.VoxelMat.vox_glslf
 *  Copyright (C) 2006-2015 Copyleft Games Group
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, see http://www.gnu.org/licenses
 *
 */

// https://imdoingitwrong.wordpress.com/2011/02/10/improved-light-attenuation/

precision mediump float;
uniform vec4 scene_ambient;
uniform vec4 light_diffuse[8], light_specular[8];
uniform vec4 mat_emission;
uniform float mat_shininess;
uniform vec3 light_pos[8];
uniform vec3 light_dir[8];
uniform int light_type[8];
uniform int light_num;
varying vec3 vVertex, vNormal;
varying float vAO;
varying vec4 vColor;
uniform vec4 fog_color;

void main() {
    vec3 n = normalize(vNormal);
    vec3 v = normalize(-vVertex);

    vec4 total = vColor * scene_ambient + mat_emission;

    for (int i = 0; i < 8; i++) {
        if (i >= light_num) {break;}

        vec3 l;
        float attenuation = 1.0;

        //Directional Light Source
        if(light_type[i] == 1){
            l = normalize(light_dir[i]);
        }
        //Point Light Source
        else{
            l = light_pos[i]-vVertex;
            float d = length(l);
            if (d > 20.0) {continue;}
            l /= d;
            float d2 = d/(1.0-pow(d/20.0,2.0));
            attenuation = 1.0/pow(1.0 + 0.01*d2,2.0);
        }

        vec3 h = normalize(l+v);
        vec4 diffuse = max(dot(l, n), 0.0) * vColor *
                    light_diffuse[i];
        vec4 specular = pow(max(dot(n, h), 0.0), mat_shininess)
                        * vColor * light_specular[i];
        total += (diffuse+specular)*attenuation;
        total = clamp(total, 0.0, 1.0);
    }

    // FIXME: Parametrize this
    float fogStart = 0.0;
    float fogWidth = 20.0;
    float fogContrib = 0.0;
    vec3 fogColor = fog_color.xyz*fog_color.w;
    if(fog_color.w > 0.0) fogContrib = max(0.0,(-vVertex.z-fogStart)/fogWidth);

    gl_FragColor = vec4(total.xyz*(1.0-fogContrib)*vAO + fogColor*fogContrib, total.w);
}
