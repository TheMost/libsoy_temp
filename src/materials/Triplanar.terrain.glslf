/*
 *  libsoy - soy.materials.Triplanar.terrain_glslf
 *  Copyright (C) 2006-2015 Copyleft Games Group
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, see http://www.gnu.org/licenses
 *
 */

// https://imdoingitwrong.wordpress.com/2011/02/10/improved-light-attenuation/

precision mediump float;
uniform vec4 scene_ambient;
uniform vec4 light_diffuse[8], light_specular[8];
uniform vec4 mat_ambient, mat_diffuse, mat_specular;
uniform vec4 mat_emission;
uniform float mat_shininess;
uniform sampler2D colormap, glowmap, bumpmap;
uniform sampler2D tNormal;
uniform samplerCube colormap_cube, glowmap_cube, bumpmap_cube;
uniform vec3 light_pos[8];
uniform vec3 light_dir[8];
uniform int light_type[8];
uniform int light_num;
varying vec4 vPos, vNor, vTan;
varying vec3 vVertex, vVertexRaw, vNormal, vTangent;
varying vec2 vTexCoordColor, vTexCoordGlow, vTexCoordBump;
uniform vec4 fog_color;

vec3 getTriPlanarBlend(vec3 wNorm){
    // in wNorm is the world-space normal of the fragment
    vec3 blending = abs( wNorm );
    blending = normalize(max(blending, 0.00001)); // Force weights to sum to 1.0
    float b = (blending.x + blending.y + blending.z);
    blending /= vec3(b, b, b);
    return blending;
}


void main() {
    float repeat = 0.5, scale = 1.0;
    vec3 blending = getTriPlanarBlend(vNor.xyz );
    vec4 xaxis = texture2D( colormap, vPos.yz * repeat);
    vec4 yaxis = texture2D( colormap, vPos.xz * repeat);
    vec4 zaxis = texture2D( colormap, vPos.xy * repeat);

    vec4 xaxis_n = texture2D( bumpmap, vPos.yz * repeat);
    vec4 yaxis_n = texture2D( bumpmap, vPos.xz * repeat);
    vec4 zaxis_n = texture2D( bumpmap, vPos.xy * repeat);

    //---------------BLENDING 1---------------------//
    float ths = 0.50;
    if(blending.y > ths){
        blending.y = 1.0;
        blending.x = 0.0;
        blending.z = 0.0;
    }
    if(blending.z > ths){
        blending.z = 1.0;
        blending.x = 0.0;
        blending.y = 0.0;
    }
    if(blending.x > ths){
        blending.x = 1.0;
        blending.y = 0.0;
        blending.z = 0.0;
    }
    
    //-----------------Blending 2 ----------------------//
    float th = 0.20;
    if(blending.x < th){
        blending.y += (blending.x)/2.0; 
        blending.z += (blending.x)/2.0;
        blending.x = 0.0;
    }
    if(blending.y < th){
        blending.x += (blending.y)/2.0; 
        blending.z += (blending.y)/2.0;
        blending.y = 0.0;
    }
    if(blending.z < th){
        blending.x += (blending.z)/2.0; 
        blending.y += (blending.z)/2.0;
        blending.z = 0.0;
    }

    // blend the results of the 3 planar projections
    vec4 colorTex = xaxis * blending.x + yaxis * blending.y + zaxis * blending.z;
    vec4 bumpTex = xaxis_n * blending.x + yaxis_n * blending.y + zaxis_n * blending.z;
    //colorTex = colorTex * 2.0 - 1.0;
    colorTex *= scale;
    bumpTex *= scale;
    //colorTex = normalize( colorTex );

    vec3 texN = vec3(textureCube(bumpmap_cube, normalize(
                vVertexRaw))+ bumpTex)* 2.0 - 1.0;

    vec3 tangentN = normalize(vNormal);
    vec3 tangentT = normalize(vTangent);
    vec3 tangentB = normalize(cross(tangentN,tangentT));
    vec3 n = mat3(tangentT,tangentB,tangentN) * texN;
    vec3 v = normalize(-vVertex);

    vec4 totalDiffuse = mat_ambient * scene_ambient;
    vec4 totalSpecular = vec4(0.0);

    // FIXME raising loop count breaks on RPI
    for (int i = 0; i < 4; i++) {
        if (i >= light_num) {break;}

        vec3 l;
        float attenuation = 1.0;

        //Directional Light Source
        if(light_type[i] == 1){
            l = normalize(light_dir[i]);
        }
        //Point Light Source
        else{
            l = light_pos[i]-vVertex;
            float d = length(l);
            if (d > 20.0) {continue;}
            l /= d;
            float d2 = d/(1.0-pow(d/20.0,2.0));
            attenuation = 1.0/pow(1.0 + 0.01*d2,2.0);
        }

        vec3 h = normalize(l+v);

        totalDiffuse += max(dot(l, n), 0.0) * mat_diffuse *
                        light_diffuse[i] * attenuation;
        totalSpecular += pow(max(dot(n, h), 0.0), mat_shininess)
                            * mat_specular * light_specular[i] *
                            attenuation;
    }

    vec3 vertexNorm = normalize(vVertexRaw);
    vec4 total = (totalDiffuse * (textureCube(colormap_cube
                    ,vertexNorm) + colorTex )+ totalSpecular) +
                    mat_emission + textureCube(glowmap_cube,
                    vertexNorm) + texture2D(glowmap,
                    vTexCoordGlow);

    // FIXME: Parametrize this
    float fogStart = 0.0;
    float fogWidth = 20.0;
    float fogContrib = 0.0;
    vec3 fogColor = fog_color.xyz*fog_color.w;

    if(fog_color.w > 0.0) {
        fogContrib = max(0.0,(-vVertex.z-fogStart)/fogWidth);
    }

    gl_FragColor = vec4(total.xyz*(1.0-fogContrib) + fogColor*fogContrib, total.w);
}


