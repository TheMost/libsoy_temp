/*
 *  libsoy - soy.fields.Buoyancy
 *  Copyright (C) 2006-2015 Copyleft Games Group
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, see http://www.gnu.org/licenses
 *
 */

[indent=4]
uses
    GLib
    ode



class soy.fields.Buoyancy : soy.fields.Field

    //acceleration required for buoyancy to work
    _acceleration: soy.fields.Accelerate
    _viscos_force : soy.fields.Wind

    init
        self._density = 1

    construct(acc : soy.fields.Accelerate)
        self._acceleration = acc
        _viscos_force = new soy.fields.Wind(self._density*2,new soy.atoms.Vector(0,0,0))

    // This function is applied once per physics cycle for each combination of affected body and field.
    // This function is applied once per physics cycle for each combination of affected body and field.
    def override exert(other : soy.bodies.Body) : bool
        //if the body is to far away don't do anything and return
        if(!(other.body_radius()>other.position.y-ry2))
            return false;
        // Volume-less bodies are not affected
        if other.volume() == 0.0f
            return false
        base_force :float =  other.get_submerged_volume(ry2,ry1)*self._density
        force : soy.atoms.Vector = new soy.atoms.Vector(
                                       -base_force*_acceleration.acceleration.x,
                                       -base_force*_acceleration.acceleration.y,
                                       -base_force*_acceleration.acceleration.z
                                                        )
        other.addForce(force.x,force.y,force.z)
        radius : soy.atoms.Vector = new soy.atoms.Vector(
                                            other.get_cm().x-other.get_cb().x,
                                            other.get_cm().y-other.get_cb().y,
                                            other.get_cm().z-other.get_cb().z
                                            )
        torque : soy.atoms.Vector = radius.cross(force)
        if !(force.x==0 && force.y==0 && force.z==0)
            other.addTorque(-torque.x,-torque.y,-torque.z)
        self._viscos_force.exert(other)
        return true

    def override region_changed()
        _viscos_force.addRegion(rx1,rx2,ry1,ry2,rz1,rz2)

    def override position_changed(value : soy.atoms.Position?)
        _viscos_force.position = value
    //
    // Properties

    // Buoyancy density
    // density of the bouyant fluid
    _density : float
    prop density : float
        get
            return self._density
        set
            self._viscos_force.density = 2*value
            self._density = value

    prop acceleration : soy.fields.Accelerate
        get
            return self._acceleration
        set
            self._acceleration = value
