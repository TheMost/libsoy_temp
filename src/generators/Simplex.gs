/*
 *  libsoy - soy.scenes.Voxelization
 *  Copyright (C) 2006-2015 Copyleft Games Group
 *
 *  This program is free software you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program if not, see http:/www.gnu.org/licenses
 *
 */

[indent=4]
uses
    GLib
    GLib.Math
    GL
    Gee
    ode

class soy.generators.Simplex : Object
    _perm : array of int
    _generator : GLib.Rand?

    init
        _seed = 0
        _min = 0.0f
        _max = 0.0f
        _scale = 0.0f
        _voxel = new soy.atoms.Voxel(new soy.atoms.Color.named("red"))
        _perm = new array of int[512]
        _generator = null

    construct(seed : int, min : float, max : float, scale : float, voxel : soy.atoms.Voxel)
        this._min = min
        this._max = max
        this._scale = scale
        this._seed = seed
        this._voxel = new soy.atoms.Voxel(voxel.color)
        this._generator = new GLib.Rand.with_seed(this._seed)
        for var i = 0 to 255
            _perm[i] = i
        for var i = 0 to 255
            j : int = _generator.int_range(0,256)
            aux : int = _perm[i]
            _perm[i] = _perm[j]
            _perm[j] = aux
        for var i = 0 to 255
            _perm[i+256] = _perm[i]

    //returns 3D simplex noise value for given coordinates.
    //return is a voxel if the simplex noise value is > 0, null otherwise
    def val(x : float, y : float, z : float) : soy.atoms.Voxel?
        x = x/scale
        y = y/scale
        z = z/scale

        n0 : float
        n1 : float
        n2 : float
        n3 : float

        F3 : float = 1.0f/3.0f
        s : float = (x+y+z)*F3
        i : int = fastfloor(x+s)
        j : int = fastfloor(y+s)
        k : int = fastfloor(z+s)

        G3 : float = 1.0f/6.0f
        t : float = (i+j+k)*G3
        X0 : float = i-t
        Y0 : float = j-t
        Z0 : float = k-t
        x0 : float = x-X0
        y0 : float = y-Y0
        z0 : float = z-Z0

        i1 : int
        j1 : int
        k1 : int

        i2 : int
        j2 : int
        k2 : int

        if x0 >= y0
            if y0 >= z0
                i1=1
                j1=0
                k1=0
                i2=1
                j2=1
                k2=0
            else if x0 >= z0
                i1=1
                j1=0
                k1=0
                i2=1
                j2=0
                k2=1
            else
                i1=0
                j1=0
                k1=1
                i2=1
                j2=0
                k2=1
        else
            if y0 < z0
                i1=0
                j1=0
                k1=1
                i2=0
                j2=1
                k2=1
            else if x0 < z0
                i1=0
                j1=1
                k1=0
                i2=0
                j2=1
                k2=1
            else
                i1=0
                j1=1
                k1=0
                i2=1
                j2=1
                k2=0

        x1 : float = x0 - i1 + G3
        y1 : float = y0 - j1 + G3
        z1 : float = z0 - k1 + G3
        x2 : float = x0 - i2 + 2.0f*G3
        y2 : float = y0 - j2 + 2.0f*G3
        z2 : float = z0 - k2 + 2.0f*G3
        x3 : float = x0 - 1.0f + 3.0f*G3
        y3 : float = y0 - 1.0f + 3.0f*G3
        z3 : float = z0 - 1.0f + 3.0f*G3

        ii : int = i & 255
        jj : int = j & 255
        kk : int = k & 255
        gi0 : int = _perm[ii+_perm[jj+_perm[kk]]] % 12
        gi1 : int = _perm[ii+i1+_perm[jj+j1+_perm[kk+k1]]] % 12
        gi2 : int = _perm[ii+i2+_perm[jj+j2+_perm[kk+k2]]] % 12
        gi3 : int = _perm[ii+1+_perm[jj+1+_perm[kk+1]]] % 12

        t0 : float = 0.6f - x0*x0 - y0*y0 - z0*z0
        if t0 < 0
            n0 = 0.0f
        else
            t0 *= t0
            n0 = t0 * t0 * dot(gi0, x0, y0, z0)

        t1 : float = 0.6f - x1*x1 - y1*y1 - z1*z1
        if t1 < 0
            n1 = 0.0f
        else
            t1 *= t1
            n1 = t1 * t1 * dot(gi1, x1, y1, z1)

        t2 : float = 0.6f - x2*x2 - y2*y2 - z2*z2
        if t2 < 0
            n2 = 0.0f
        else
            t2 *= t2
            n2 = t2 * t2 * dot(gi2, x2, y2, z2)

        t3 : float = 0.6f - x3*x3 - y3*y3 - z3*z3
        if t3 < 0
            n3 = 0.0f
        else
            t3 *= t3
            n3 = t3 * t3 * dot(gi3, x3, y3, z3)

        result : float = 32.0f*(n0 + n1 + n2 + n3)

        result += 1
        result *= 0.5f
        difference : float = this._max-this._min
        if (this._min + difference * result) < 0 do return null
        return this._voxel

    def fastfloor(x : float) : int
        return x > 0 ? (int) x : ((int) x) - 1

    def dot(g : int, x : float, y : float, z : float) : float
        return grad3[g,0]*x + grad3[g,1]*y + grad3[g,2]*z

    _seed : int
    prop seed : int
        get
            return _seed
        set
            self._seed = value

    _min : float
    prop min : float
        get
            return _min
        set
            self._min = value

    _max : float
    prop max : float
        get
            return _max
        set
            self._max = value

    _scale : float
    prop scale : float
        get
            return _scale
        set
            self._scale = value

    _voxel : soy.atoms.Voxel?
    prop voxel : soy.atoms.Voxel?
        get
            return _voxel
        set
            self._voxel = value

    grad3 : static array of int[,]

    init static
        grad3 = {
            {1,1,0}, {-1,1,0}, {1,-1,0}, {-1,-1,0},
            {1,0,1}, {-1,0,1}, {1,0,-1}, {-1,0,-1},
            {0,1,1}, {0,-1,1}, {0,1,-1}, {0,-1,-1}
        }
