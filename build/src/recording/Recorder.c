/* Recorder.c generated by valac 0.34.8, the Vala compiler
 * generated from Recorder.gs, do not modify */

/*
 *  libsoy - soy.recording.Recorder
 *  Copyright (C) 2006-2015 Copyleft Games Group
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, see http://www.gnu.org/licenses
 *
 */

#include <glib.h>
#include <glib-object.h>
#include <gee.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <gio/gio.h>
#include <glib/gstdio.h>


#define SOY_RECORDING_TYPE_RECORDER (soy_recording_recorder_get_type ())
#define SOY_RECORDING_RECORDER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), SOY_RECORDING_TYPE_RECORDER, soyrecordingRecorder))
#define SOY_RECORDING_RECORDER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), SOY_RECORDING_TYPE_RECORDER, soyrecordingRecorderClass))
#define SOY_RECORDING_IS_RECORDER(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), SOY_RECORDING_TYPE_RECORDER))
#define SOY_RECORDING_IS_RECORDER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), SOY_RECORDING_TYPE_RECORDER))
#define SOY_RECORDING_RECORDER_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), SOY_RECORDING_TYPE_RECORDER, soyrecordingRecorderClass))

typedef struct _soyrecordingRecorder soyrecordingRecorder;
typedef struct _soyrecordingRecorderClass soyrecordingRecorderClass;
typedef struct _soyrecordingRecorderPrivate soyrecordingRecorderPrivate;

#define SOY_RECORDING_TYPE_GL_FUNC (soy_recording_gl_func_get_type ())

#define SOY_RECORDING_TYPE_RECORDER_MODULE_STATE (soy_recording_recorder_module_state_get_type ())
#define _g_object_unref0(var) ((var == NULL) ? NULL : (var = (g_object_unref (var), NULL)))
#define _fclose0(var) ((var == NULL) ? NULL : (var = (fclose (var), NULL)))
#define _g_free0(var) (var = (g_free (var), NULL))

struct _soyrecordingRecorder {
	GObject parent_instance;
	soyrecordingRecorderPrivate * priv;
};

struct _soyrecordingRecorderClass {
	GObjectClass parent_class;
};

typedef enum  {
	SOY_RECORDING_GL_FUNC_glActiveTexture,
	SOY_RECORDING_GL_FUNC_glAttachShader,
	SOY_RECORDING_GL_FUNC_glBindAttribLocation,
	SOY_RECORDING_GL_FUNC_glBindBuffer,
	SOY_RECORDING_GL_FUNC_glBindFramebuffer,
	SOY_RECORDING_GL_FUNC_glBindRenderbuffer,
	SOY_RECORDING_GL_FUNC_glBindTexture,
	SOY_RECORDING_GL_FUNC_glBlendColor,
	SOY_RECORDING_GL_FUNC_glBlendEquation,
	SOY_RECORDING_GL_FUNC_glBlendEquationSeparate,
	SOY_RECORDING_GL_FUNC_glBlendFunc,
	SOY_RECORDING_GL_FUNC_glBlendFuncSeparate,
	SOY_RECORDING_GL_FUNC_glBufferData,
	SOY_RECORDING_GL_FUNC_glBufferSubData,
	SOY_RECORDING_GL_FUNC_glCheckFramebufferStatus,
	SOY_RECORDING_GL_FUNC_glClear,
	SOY_RECORDING_GL_FUNC_glClearColor,
	SOY_RECORDING_GL_FUNC_glClearDepthf,
	SOY_RECORDING_GL_FUNC_glClearStencil,
	SOY_RECORDING_GL_FUNC_glColorMask,
	SOY_RECORDING_GL_FUNC_glCompileShader,
	SOY_RECORDING_GL_FUNC_glCompressedTexImage2D,
	SOY_RECORDING_GL_FUNC_glCompressedTexSubImage2D,
	SOY_RECORDING_GL_FUNC_glCopyTexImage2D,
	SOY_RECORDING_GL_FUNC_glCopyTexSubImage2D,
	SOY_RECORDING_GL_FUNC_glCreateProgram,
	SOY_RECORDING_GL_FUNC_glCreateShader,
	SOY_RECORDING_GL_FUNC_glCullFace,
	SOY_RECORDING_GL_FUNC_glDeleteBuffers,
	SOY_RECORDING_GL_FUNC_glDeleteFramebuffers,
	SOY_RECORDING_GL_FUNC_glDeleteProgram,
	SOY_RECORDING_GL_FUNC_glDeleteRenderbuffers,
	SOY_RECORDING_GL_FUNC_glDeleteShader,
	SOY_RECORDING_GL_FUNC_glDeleteTextures,
	SOY_RECORDING_GL_FUNC_glDepthFunc,
	SOY_RECORDING_GL_FUNC_glDepthMask,
	SOY_RECORDING_GL_FUNC_glDepthRangef,
	SOY_RECORDING_GL_FUNC_glDetachShader,
	SOY_RECORDING_GL_FUNC_glDisable,
	SOY_RECORDING_GL_FUNC_glDisableVertexAttribArray,
	SOY_RECORDING_GL_FUNC_glDrawArrays,
	SOY_RECORDING_GL_FUNC_glDrawElements,
	SOY_RECORDING_GL_FUNC_glEnable,
	SOY_RECORDING_GL_FUNC_glEnableVertexAttribArray,
	SOY_RECORDING_GL_FUNC_glFinish,
	SOY_RECORDING_GL_FUNC_glFlush,
	SOY_RECORDING_GL_FUNC_glFramebufferRenderbuffer,
	SOY_RECORDING_GL_FUNC_glFramebufferTexture2D,
	SOY_RECORDING_GL_FUNC_glFrontFace,
	SOY_RECORDING_GL_FUNC_glGenBuffers,
	SOY_RECORDING_GL_FUNC_glGenerateMipmap,
	SOY_RECORDING_GL_FUNC_glGenFramebuffers,
	SOY_RECORDING_GL_FUNC_glGenRenderbuffers,
	SOY_RECORDING_GL_FUNC_glGenTextures,
	SOY_RECORDING_GL_FUNC_glGetActiveAttrib,
	SOY_RECORDING_GL_FUNC_glGetActiveUniform,
	SOY_RECORDING_GL_FUNC_glGetAttachedShaders,
	SOY_RECORDING_GL_FUNC_glGetAttribLocation,
	SOY_RECORDING_GL_FUNC_glGetBooleanv,
	SOY_RECORDING_GL_FUNC_glGetBufferParameteriv,
	SOY_RECORDING_GL_FUNC_glGetError,
	SOY_RECORDING_GL_FUNC_glGetFloatv,
	SOY_RECORDING_GL_FUNC_glGetFramebufferAttachmentParameteriv,
	SOY_RECORDING_GL_FUNC_glGetIntegerv,
	SOY_RECORDING_GL_FUNC_glGetProgramiv,
	SOY_RECORDING_GL_FUNC_glGetProgramInfoLog,
	SOY_RECORDING_GL_FUNC_glGetRenderbufferParameteriv,
	SOY_RECORDING_GL_FUNC_glGetShaderiv,
	SOY_RECORDING_GL_FUNC_glGetShaderInfoLog,
	SOY_RECORDING_GL_FUNC_glGetShaderPrecisionFormat,
	SOY_RECORDING_GL_FUNC_glGetShaderSource,
	SOY_RECORDING_GL_FUNC_glGetString,
	SOY_RECORDING_GL_FUNC_glGetTexParameterfv,
	SOY_RECORDING_GL_FUNC_glGetTexParameteriv,
	SOY_RECORDING_GL_FUNC_glGetUniformfv,
	SOY_RECORDING_GL_FUNC_glGetUniformiv,
	SOY_RECORDING_GL_FUNC_glGetUniformLocation,
	SOY_RECORDING_GL_FUNC_glGetVertexAttribfv,
	SOY_RECORDING_GL_FUNC_glGetVertexAttribiv,
	SOY_RECORDING_GL_FUNC_glGetVertexAttribPointerv,
	SOY_RECORDING_GL_FUNC_glHint,
	SOY_RECORDING_GL_FUNC_glIsBuffer,
	SOY_RECORDING_GL_FUNC_glIsEnabled,
	SOY_RECORDING_GL_FUNC_glIsFramebuffer,
	SOY_RECORDING_GL_FUNC_glIsProgram,
	SOY_RECORDING_GL_FUNC_glIsRenderbuffer,
	SOY_RECORDING_GL_FUNC_glIsShader,
	SOY_RECORDING_GL_FUNC_glIsTexture,
	SOY_RECORDING_GL_FUNC_glLineWidth,
	SOY_RECORDING_GL_FUNC_glLinkProgram,
	SOY_RECORDING_GL_FUNC_glPixelStorei,
	SOY_RECORDING_GL_FUNC_glPolygonOffset,
	SOY_RECORDING_GL_FUNC_glReadPixels,
	SOY_RECORDING_GL_FUNC_glReleaseShaderCompiler,
	SOY_RECORDING_GL_FUNC_glRenderbufferStorage,
	SOY_RECORDING_GL_FUNC_glSampleCoverage,
	SOY_RECORDING_GL_FUNC_glScissor,
	SOY_RECORDING_GL_FUNC_glShaderBinary,
	SOY_RECORDING_GL_FUNC_glShaderSource,
	SOY_RECORDING_GL_FUNC_glStencilFunc,
	SOY_RECORDING_GL_FUNC_glStencilFuncSeparate,
	SOY_RECORDING_GL_FUNC_glStencilMask,
	SOY_RECORDING_GL_FUNC_glStencilMaskSeparate,
	SOY_RECORDING_GL_FUNC_glStencilOp,
	SOY_RECORDING_GL_FUNC_glStencilOpSeparate,
	SOY_RECORDING_GL_FUNC_glTexImage2D,
	SOY_RECORDING_GL_FUNC_glTexParameterf,
	SOY_RECORDING_GL_FUNC_glTexParameterfv,
	SOY_RECORDING_GL_FUNC_glTexParameteri,
	SOY_RECORDING_GL_FUNC_glTexParameteriv,
	SOY_RECORDING_GL_FUNC_glTexSubImage2D,
	SOY_RECORDING_GL_FUNC_glUniform1f,
	SOY_RECORDING_GL_FUNC_glUniform1fv,
	SOY_RECORDING_GL_FUNC_glUniform1i,
	SOY_RECORDING_GL_FUNC_glUniform1iv,
	SOY_RECORDING_GL_FUNC_glUniform2f,
	SOY_RECORDING_GL_FUNC_glUniform2fv,
	SOY_RECORDING_GL_FUNC_glUniform2i,
	SOY_RECORDING_GL_FUNC_glUniform2iv,
	SOY_RECORDING_GL_FUNC_glUniform3f,
	SOY_RECORDING_GL_FUNC_glUniform3fv,
	SOY_RECORDING_GL_FUNC_glUniform3i,
	SOY_RECORDING_GL_FUNC_glUniform3iv,
	SOY_RECORDING_GL_FUNC_glUniform4f,
	SOY_RECORDING_GL_FUNC_glUniform4fv,
	SOY_RECORDING_GL_FUNC_glUniform4i,
	SOY_RECORDING_GL_FUNC_glUniform4iv,
	SOY_RECORDING_GL_FUNC_glUniformMatrix2fv,
	SOY_RECORDING_GL_FUNC_glUniformMatrix3fv,
	SOY_RECORDING_GL_FUNC_glUniformMatrix4fv,
	SOY_RECORDING_GL_FUNC_glUseProgram,
	SOY_RECORDING_GL_FUNC_glValidateProgram,
	SOY_RECORDING_GL_FUNC_glVertexAttrib1f,
	SOY_RECORDING_GL_FUNC_glVertexAttrib1fv,
	SOY_RECORDING_GL_FUNC_glVertexAttrib2f,
	SOY_RECORDING_GL_FUNC_glVertexAttrib2fv,
	SOY_RECORDING_GL_FUNC_glVertexAttrib3f,
	SOY_RECORDING_GL_FUNC_glVertexAttrib3fv,
	SOY_RECORDING_GL_FUNC_glVertexAttrib4f,
	SOY_RECORDING_GL_FUNC_glVertexAttrib4fv,
	SOY_RECORDING_GL_FUNC_glVertexAttribPointer,
	SOY_RECORDING_GL_FUNC_glViewport,
	SOY_RECORDING_GL_FUNC_nonglINJECTDRAWWAIT,
	SOY_RECORDING_GL_FUNC_eglMakeCurrent,
	SOY_RECORDING_GL_FUNC_eglSwapBuffers
} soyrecordingGlFunc;

typedef enum  {
	SOY_RECORDING_RECORDER_MODULE_STATE_playbackMedia,
	SOY_RECORDING_RECORDER_MODULE_STATE_recordMedia,
	SOY_RECORDING_RECORDER_MODULE_STATE_doNothing
} soyrecordingRecorderModuleState;


static gpointer soy_recording_recorder_parent_class = NULL;
extern GeeArrayList* soy_recording_recorder_funcNameBuf;
GeeArrayList* soy_recording_recorder_funcNameBuf = NULL;
extern GeeArrayList* soy_recording_recorder_paramBuf;
GeeArrayList* soy_recording_recorder_paramBuf = NULL;
extern gint soy_recording_recorder_bufferFlushThresh;
gint soy_recording_recorder_bufferFlushThresh = 5000;
extern FILE* soy_recording_recorder_outputFile;
FILE* soy_recording_recorder_outputFile = NULL;
extern soyrecordingRecorderModuleState soy_recording_recorder_recorderState;
soyrecordingRecorderModuleState soy_recording_recorder_recorderState = SOY_RECORDING_RECORDER_MODULE_STATE_doNothing;
extern gboolean soy_recording_recorder_playbackStarted;
gboolean soy_recording_recorder_playbackStarted = FALSE;

GType soy_recording_recorder_get_type (void) G_GNUC_CONST;
enum  {
	SOY_RECORDING_RECORDER_DUMMY_PROPERTY
};
GType soy_recording_gl_func_get_type (void) G_GNUC_CONST;
GType soy_recording_recorder_module_state_get_type (void) G_GNUC_CONST;
#define SOY_RECORDING_RECORDER_recordingFile "game.rec"
void soy_recording_recorder_initialize (void);
void soy_recording_recorder_clearRecordingBuffers (void);
void soy_recording_recorder_writeBuffers (void);
void soy_recording_recorder_recordCall (soyrecordingGlFunc funcName, const gchar* parameters);
void soy_recording_playback_startPlayback (const gchar* recordingFile);
void soy_recording_recorder_recordPointerCall (soyrecordingGlFunc funcName, const gchar* parameters);
gchar* soy_recording_pointer_serializer_serializeDataFunction (soyrecordingGlFunc funcName, const gchar* parameters);
soyrecordingRecorder* soy_recording_recorder_new (void);
soyrecordingRecorder* soy_recording_recorder_construct (GType object_type);
static void soy_recording_recorder_finalize (GObject* obj);


void soy_recording_recorder_initialize (void) {
	GeeArrayList* _tmp0_ = NULL;
	GeeArrayList* _tmp1_ = NULL;
	GFile* file = NULL;
	GFile* _tmp2_ = NULL;
	FILE* _tmp3_ = NULL;
#line 36 "/home/bmost/libsoy/src/recording/Recorder.gs"
	g_print ("gl recording buffers initialized\n");
#line 37 "/home/bmost/libsoy/src/recording/Recorder.gs"
	_tmp0_ = gee_array_list_new (SOY_RECORDING_TYPE_GL_FUNC, NULL, NULL, NULL, NULL, NULL);
#line 37 "/home/bmost/libsoy/src/recording/Recorder.gs"
	_g_object_unref0 (soy_recording_recorder_funcNameBuf);
#line 37 "/home/bmost/libsoy/src/recording/Recorder.gs"
	soy_recording_recorder_funcNameBuf = _tmp0_;
#line 38 "/home/bmost/libsoy/src/recording/Recorder.gs"
	_tmp1_ = gee_array_list_new (G_TYPE_STRING, (GBoxedCopyFunc) g_strdup, (GDestroyNotify) g_free, NULL, NULL, NULL);
#line 38 "/home/bmost/libsoy/src/recording/Recorder.gs"
	_g_object_unref0 (soy_recording_recorder_paramBuf);
#line 38 "/home/bmost/libsoy/src/recording/Recorder.gs"
	soy_recording_recorder_paramBuf = _tmp1_;
#line 39 "/home/bmost/libsoy/src/recording/Recorder.gs"
	_tmp2_ = g_file_new_for_path (SOY_RECORDING_RECORDER_recordingFile);
#line 39 "/home/bmost/libsoy/src/recording/Recorder.gs"
	file = _tmp2_;
#line 40 "/home/bmost/libsoy/src/recording/Recorder.gs"
	_tmp3_ = g_fopen (SOY_RECORDING_RECORDER_recordingFile, "w");
#line 40 "/home/bmost/libsoy/src/recording/Recorder.gs"
	_fclose0 (soy_recording_recorder_outputFile);
#line 40 "/home/bmost/libsoy/src/recording/Recorder.gs"
	soy_recording_recorder_outputFile = _tmp3_;
#line 35 "/home/bmost/libsoy/src/recording/Recorder.gs"
	_g_object_unref0 (file);
#line 281 "Recorder.c"
}


void soy_recording_recorder_clearRecordingBuffers (void) {
	GeeArrayList* _tmp0_ = NULL;
	GeeArrayList* _tmp1_ = NULL;
#line 44 "/home/bmost/libsoy/src/recording/Recorder.gs"
	_tmp0_ = soy_recording_recorder_funcNameBuf;
#line 44 "/home/bmost/libsoy/src/recording/Recorder.gs"
	gee_abstract_collection_clear ((GeeAbstractCollection*) _tmp0_);
#line 45 "/home/bmost/libsoy/src/recording/Recorder.gs"
	_tmp1_ = soy_recording_recorder_paramBuf;
#line 45 "/home/bmost/libsoy/src/recording/Recorder.gs"
	gee_abstract_collection_clear ((GeeAbstractCollection*) _tmp1_);
#line 296 "Recorder.c"
}


void soy_recording_recorder_writeBuffers (void) {
	gint i = 0;
	FILE* _tmp22_ = NULL;
	{
		gboolean _tmp0_ = FALSE;
#line 50 "/home/bmost/libsoy/src/recording/Recorder.gs"
		i = 0;
#line 50 "/home/bmost/libsoy/src/recording/Recorder.gs"
		_tmp0_ = TRUE;
#line 50 "/home/bmost/libsoy/src/recording/Recorder.gs"
		while (TRUE) {
#line 311 "Recorder.c"
			GeeArrayList* _tmp2_ = NULL;
			gint _tmp3_ = 0;
			gint _tmp4_ = 0;
			gchar* funcStr = NULL;
			GeeArrayList* _tmp5_ = NULL;
			gint _tmp6_ = 0;
			gpointer _tmp7_ = NULL;
			gchar* _tmp8_ = NULL;
			gchar* _tmp9_ = NULL;
			gchar* _tmp10_ = NULL;
			gchar* _tmp11_ = NULL;
			GeeArrayList* _tmp12_ = NULL;
			gint _tmp13_ = 0;
			gpointer _tmp14_ = NULL;
			gchar* _tmp15_ = NULL;
			gchar* _tmp16_ = NULL;
			gchar* _tmp17_ = NULL;
			gchar* _tmp18_ = NULL;
			gchar* _tmp19_ = NULL;
			FILE* _tmp20_ = NULL;
			const gchar* _tmp21_ = NULL;
#line 50 "/home/bmost/libsoy/src/recording/Recorder.gs"
			if (!_tmp0_) {
#line 335 "Recorder.c"
				gint _tmp1_ = 0;
#line 50 "/home/bmost/libsoy/src/recording/Recorder.gs"
				_tmp1_ = i;
#line 50 "/home/bmost/libsoy/src/recording/Recorder.gs"
				i = _tmp1_ + 1;
#line 341 "Recorder.c"
			}
#line 50 "/home/bmost/libsoy/src/recording/Recorder.gs"
			_tmp0_ = FALSE;
#line 50 "/home/bmost/libsoy/src/recording/Recorder.gs"
			_tmp2_ = soy_recording_recorder_funcNameBuf;
#line 50 "/home/bmost/libsoy/src/recording/Recorder.gs"
			_tmp3_ = gee_abstract_collection_get_size ((GeeAbstractCollection*) _tmp2_);
#line 50 "/home/bmost/libsoy/src/recording/Recorder.gs"
			_tmp4_ = _tmp3_;
#line 50 "/home/bmost/libsoy/src/recording/Recorder.gs"
			if (!(i <= (_tmp4_ - 1))) {
#line 50 "/home/bmost/libsoy/src/recording/Recorder.gs"
				break;
#line 355 "Recorder.c"
			}
#line 51 "/home/bmost/libsoy/src/recording/Recorder.gs"
			_tmp5_ = soy_recording_recorder_funcNameBuf;
#line 51 "/home/bmost/libsoy/src/recording/Recorder.gs"
			_tmp6_ = i;
#line 51 "/home/bmost/libsoy/src/recording/Recorder.gs"
			_tmp7_ = gee_abstract_list_get ((GeeAbstractList*) _tmp5_, _tmp6_);
#line 51 "/home/bmost/libsoy/src/recording/Recorder.gs"
			_tmp8_ = g_strdup_printf ("%i", (gint) ((soyrecordingGlFunc) ((gintptr) _tmp7_)));
#line 51 "/home/bmost/libsoy/src/recording/Recorder.gs"
			_tmp9_ = _tmp8_;
#line 51 "/home/bmost/libsoy/src/recording/Recorder.gs"
			_tmp10_ = g_strconcat (_tmp9_, " ", NULL);
#line 51 "/home/bmost/libsoy/src/recording/Recorder.gs"
			_tmp11_ = _tmp10_;
#line 51 "/home/bmost/libsoy/src/recording/Recorder.gs"
			_tmp12_ = soy_recording_recorder_paramBuf;
#line 51 "/home/bmost/libsoy/src/recording/Recorder.gs"
			_tmp13_ = i;
#line 51 "/home/bmost/libsoy/src/recording/Recorder.gs"
			_tmp14_ = gee_abstract_list_get ((GeeAbstractList*) _tmp12_, _tmp13_);
#line 51 "/home/bmost/libsoy/src/recording/Recorder.gs"
			_tmp15_ = (gchar*) _tmp14_;
#line 51 "/home/bmost/libsoy/src/recording/Recorder.gs"
			_tmp16_ = g_strconcat (_tmp11_, _tmp15_, NULL);
#line 51 "/home/bmost/libsoy/src/recording/Recorder.gs"
			_tmp17_ = _tmp16_;
#line 51 "/home/bmost/libsoy/src/recording/Recorder.gs"
			_tmp18_ = g_strconcat (_tmp17_, "\r\n", NULL);
#line 51 "/home/bmost/libsoy/src/recording/Recorder.gs"
			_tmp19_ = _tmp18_;
#line 51 "/home/bmost/libsoy/src/recording/Recorder.gs"
			_g_free0 (_tmp17_);
#line 51 "/home/bmost/libsoy/src/recording/Recorder.gs"
			_g_free0 (_tmp15_);
#line 51 "/home/bmost/libsoy/src/recording/Recorder.gs"
			_g_free0 (_tmp11_);
#line 51 "/home/bmost/libsoy/src/recording/Recorder.gs"
			_g_free0 (_tmp9_);
#line 51 "/home/bmost/libsoy/src/recording/Recorder.gs"
			funcStr = _tmp19_;
#line 52 "/home/bmost/libsoy/src/recording/Recorder.gs"
			_tmp20_ = soy_recording_recorder_outputFile;
#line 52 "/home/bmost/libsoy/src/recording/Recorder.gs"
			_tmp21_ = funcStr;
#line 52 "/home/bmost/libsoy/src/recording/Recorder.gs"
			fputs (_tmp21_, _tmp20_);
#line 50 "/home/bmost/libsoy/src/recording/Recorder.gs"
			_g_free0 (funcStr);
#line 405 "Recorder.c"
		}
	}
#line 54 "/home/bmost/libsoy/src/recording/Recorder.gs"
	_tmp22_ = soy_recording_recorder_outputFile;
#line 54 "/home/bmost/libsoy/src/recording/Recorder.gs"
	fflush (_tmp22_);
#line 55 "/home/bmost/libsoy/src/recording/Recorder.gs"
	soy_recording_recorder_clearRecordingBuffers ();
#line 414 "Recorder.c"
}


void soy_recording_recorder_recordCall (soyrecordingGlFunc funcName, const gchar* parameters) {
	gboolean _tmp0_ = FALSE;
	soyrecordingRecorderModuleState _tmp1_ = 0;
	soyrecordingRecorderModuleState _tmp2_ = 0;
#line 58 "/home/bmost/libsoy/src/recording/Recorder.gs"
	g_return_if_fail (parameters != NULL);
#line 62 "/home/bmost/libsoy/src/recording/Recorder.gs"
	_tmp0_ = soy_recording_recorder_playbackStarted;
#line 62 "/home/bmost/libsoy/src/recording/Recorder.gs"
	if (!_tmp0_) {
#line 63 "/home/bmost/libsoy/src/recording/Recorder.gs"
		soy_recording_playback_startPlayback ("game.rec");
#line 64 "/home/bmost/libsoy/src/recording/Recorder.gs"
		soy_recording_recorder_playbackStarted = TRUE;
#line 432 "Recorder.c"
	}
#line 65 "/home/bmost/libsoy/src/recording/Recorder.gs"
	_tmp1_ = soy_recording_recorder_recorderState;
#line 65 "/home/bmost/libsoy/src/recording/Recorder.gs"
	if (_tmp1_ == SOY_RECORDING_RECORDER_MODULE_STATE_playbackMedia) {
#line 66 "/home/bmost/libsoy/src/recording/Recorder.gs"
		return;
#line 440 "Recorder.c"
	}
#line 67 "/home/bmost/libsoy/src/recording/Recorder.gs"
	_tmp2_ = soy_recording_recorder_recorderState;
#line 67 "/home/bmost/libsoy/src/recording/Recorder.gs"
	if (_tmp2_ == SOY_RECORDING_RECORDER_MODULE_STATE_recordMedia) {
#line 446 "Recorder.c"
		GeeArrayList* _tmp3_ = NULL;
		GeeArrayList* _tmp4_ = NULL;
		soyrecordingGlFunc _tmp5_ = 0;
		GeeArrayList* _tmp6_ = NULL;
		const gchar* _tmp7_ = NULL;
		GeeArrayList* _tmp8_ = NULL;
		gint _tmp9_ = 0;
		gint _tmp10_ = 0;
		gint _tmp11_ = 0;
#line 68 "/home/bmost/libsoy/src/recording/Recorder.gs"
		_tmp3_ = soy_recording_recorder_funcNameBuf;
#line 68 "/home/bmost/libsoy/src/recording/Recorder.gs"
		if (_tmp3_ == NULL) {
#line 69 "/home/bmost/libsoy/src/recording/Recorder.gs"
			soy_recording_recorder_initialize ();
#line 462 "Recorder.c"
		}
#line 70 "/home/bmost/libsoy/src/recording/Recorder.gs"
		_tmp4_ = soy_recording_recorder_funcNameBuf;
#line 70 "/home/bmost/libsoy/src/recording/Recorder.gs"
		_tmp5_ = funcName;
#line 70 "/home/bmost/libsoy/src/recording/Recorder.gs"
		gee_abstract_collection_add ((GeeAbstractCollection*) _tmp4_, (gpointer) ((gintptr) _tmp5_));
#line 71 "/home/bmost/libsoy/src/recording/Recorder.gs"
		_tmp6_ = soy_recording_recorder_paramBuf;
#line 71 "/home/bmost/libsoy/src/recording/Recorder.gs"
		_tmp7_ = parameters;
#line 71 "/home/bmost/libsoy/src/recording/Recorder.gs"
		gee_abstract_collection_add ((GeeAbstractCollection*) _tmp6_, _tmp7_);
#line 75 "/home/bmost/libsoy/src/recording/Recorder.gs"
		_tmp8_ = soy_recording_recorder_funcNameBuf;
#line 75 "/home/bmost/libsoy/src/recording/Recorder.gs"
		_tmp9_ = gee_abstract_collection_get_size ((GeeAbstractCollection*) _tmp8_);
#line 75 "/home/bmost/libsoy/src/recording/Recorder.gs"
		_tmp10_ = _tmp9_;
#line 75 "/home/bmost/libsoy/src/recording/Recorder.gs"
		_tmp11_ = soy_recording_recorder_bufferFlushThresh;
#line 75 "/home/bmost/libsoy/src/recording/Recorder.gs"
		if (_tmp10_ > _tmp11_) {
#line 76 "/home/bmost/libsoy/src/recording/Recorder.gs"
			soy_recording_recorder_writeBuffers ();
#line 488 "Recorder.c"
		}
	}
#line 77 "/home/bmost/libsoy/src/recording/Recorder.gs"
	return;
#line 493 "Recorder.c"
}


void soy_recording_recorder_recordPointerCall (soyrecordingGlFunc funcName, const gchar* parameters) {
	gchar* expandedParams = NULL;
	soyrecordingGlFunc _tmp0_ = 0;
	const gchar* _tmp1_ = NULL;
	gchar* _tmp2_ = NULL;
	soyrecordingGlFunc _tmp3_ = 0;
#line 79 "/home/bmost/libsoy/src/recording/Recorder.gs"
	g_return_if_fail (parameters != NULL);
#line 80 "/home/bmost/libsoy/src/recording/Recorder.gs"
	_tmp0_ = funcName;
#line 80 "/home/bmost/libsoy/src/recording/Recorder.gs"
	_tmp1_ = parameters;
#line 80 "/home/bmost/libsoy/src/recording/Recorder.gs"
	_tmp2_ = soy_recording_pointer_serializer_serializeDataFunction (_tmp0_, _tmp1_);
#line 80 "/home/bmost/libsoy/src/recording/Recorder.gs"
	expandedParams = _tmp2_;
#line 81 "/home/bmost/libsoy/src/recording/Recorder.gs"
	_tmp3_ = funcName;
#line 81 "/home/bmost/libsoy/src/recording/Recorder.gs"
	soy_recording_recorder_recordCall (_tmp3_, expandedParams);
#line 79 "/home/bmost/libsoy/src/recording/Recorder.gs"
	_g_free0 (expandedParams);
#line 519 "Recorder.c"
}


soyrecordingRecorder* soy_recording_recorder_construct (GType object_type) {
	soyrecordingRecorder * self = NULL;
#line 23 "/home/bmost/libsoy/src/recording/Recorder.gs"
	self = (soyrecordingRecorder*) g_object_new (object_type, NULL);
#line 23 "/home/bmost/libsoy/src/recording/Recorder.gs"
	return self;
#line 529 "Recorder.c"
}


soyrecordingRecorder* soy_recording_recorder_new (void) {
#line 23 "/home/bmost/libsoy/src/recording/Recorder.gs"
	return soy_recording_recorder_construct (SOY_RECORDING_TYPE_RECORDER);
#line 536 "Recorder.c"
}


static void soy_recording_recorder_class_init (soyrecordingRecorderClass * klass) {
#line 23 "/home/bmost/libsoy/src/recording/Recorder.gs"
	soy_recording_recorder_parent_class = g_type_class_peek_parent (klass);
#line 23 "/home/bmost/libsoy/src/recording/Recorder.gs"
	G_OBJECT_CLASS (klass)->finalize = soy_recording_recorder_finalize;
#line 545 "Recorder.c"
}


static void soy_recording_recorder_instance_init (soyrecordingRecorder * self) {
}


static void soy_recording_recorder_finalize (GObject* obj) {
	soyrecordingRecorder * self;
#line 23 "/home/bmost/libsoy/src/recording/Recorder.gs"
	self = G_TYPE_CHECK_INSTANCE_CAST (obj, SOY_RECORDING_TYPE_RECORDER, soyrecordingRecorder);
#line 23 "/home/bmost/libsoy/src/recording/Recorder.gs"
	G_OBJECT_CLASS (soy_recording_recorder_parent_class)->finalize (obj);
#line 559 "Recorder.c"
}


GType soy_recording_recorder_get_type (void) {
	static volatile gsize soy_recording_recorder_type_id__volatile = 0;
	if (g_once_init_enter (&soy_recording_recorder_type_id__volatile)) {
		static const GTypeInfo g_define_type_info = { sizeof (soyrecordingRecorderClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) soy_recording_recorder_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (soyrecordingRecorder), 0, (GInstanceInitFunc) soy_recording_recorder_instance_init, NULL };
		GType soy_recording_recorder_type_id;
		soy_recording_recorder_type_id = g_type_register_static (G_TYPE_OBJECT, "soyrecordingRecorder", &g_define_type_info, 0);
		g_once_init_leave (&soy_recording_recorder_type_id__volatile, soy_recording_recorder_type_id);
	}
	return soy_recording_recorder_type_id__volatile;
}



