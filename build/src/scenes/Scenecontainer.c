/* Scenecontainer.c generated by valac 0.34.8, the Vala compiler
 * generated from Scenecontainer.gs, do not modify */

/*
 *  libsoy - soy.scenes.Scenecontainer
 *  Copyright (C) 2006-2015 Copyleft Games Group
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program; if not, see http://www.gnu.org/licenses
 *
 */

#include <glib.h>
#include <glib-object.h>
#include <gee.h>
#include <stdlib.h>
#include <string.h>


#define SOY_SCENES_TYPE_SCENECONTAINER (soy_scenes_scenecontainer_get_type ())
#define SOY_SCENES_SCENECONTAINER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), SOY_SCENES_TYPE_SCENECONTAINER, soyscenesScenecontainer))
#define SOY_SCENES_SCENECONTAINER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), SOY_SCENES_TYPE_SCENECONTAINER, soyscenesScenecontainerClass))
#define SOY_SCENES_IS_SCENECONTAINER(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), SOY_SCENES_TYPE_SCENECONTAINER))
#define SOY_SCENES_IS_SCENECONTAINER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), SOY_SCENES_TYPE_SCENECONTAINER))
#define SOY_SCENES_SCENECONTAINER_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), SOY_SCENES_TYPE_SCENECONTAINER, soyscenesScenecontainerClass))

typedef struct _soyscenesScenecontainer soyscenesScenecontainer;
typedef struct _soyscenesScenecontainerClass soyscenesScenecontainerClass;
typedef struct _soyscenesScenecontainerPrivate soyscenesScenecontainerPrivate;

#define SOY_SCENES_TYPE_SCENE (soy_scenes_scene_get_type ())
#define SOY_SCENES_SCENE(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), SOY_SCENES_TYPE_SCENE, soyscenesScene))
#define SOY_SCENES_SCENE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), SOY_SCENES_TYPE_SCENE, soyscenesSceneClass))
#define SOY_SCENES_IS_SCENE(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), SOY_SCENES_TYPE_SCENE))
#define SOY_SCENES_IS_SCENE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), SOY_SCENES_TYPE_SCENE))
#define SOY_SCENES_SCENE_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), SOY_SCENES_TYPE_SCENE, soyscenesSceneClass))

typedef struct _soyscenesScene soyscenesScene;
typedef struct _soyscenesSceneClass soyscenesSceneClass;
#define _g_object_unref0(var) ((var == NULL) ? NULL : (var = (g_object_unref (var), NULL)))

struct _soyscenesScenecontainer {
	GObject parent_instance;
	soyscenesScenecontainerPrivate * priv;
	GeeHashMap* scenes;
	GMutex mutex;
};

struct _soyscenesScenecontainerClass {
	GObjectClass parent_class;
};


static gpointer soy_scenes_scenecontainer_parent_class = NULL;
extern GRWLock soy_scenes__stepLock;

GType soy_scenes_scenecontainer_get_type (void) G_GNUC_CONST;
GType soy_scenes_scene_get_type (void) G_GNUC_CONST;
enum  {
	SOY_SCENES_SCENECONTAINER_DUMMY_PROPERTY,
	SOY_SCENES_SCENECONTAINER_LENGTH
};
soyscenesScenecontainer* soy_scenes_scenecontainer_new (void);
soyscenesScenecontainer* soy_scenes_scenecontainer_construct (GType object_type);
soyscenesScene* soy_scenes_scenecontainer_get (soyscenesScenecontainer* self, const gchar* key);
gboolean soy_scenes_scenecontainer_has_key (soyscenesScenecontainer* self, const gchar* key);
void soy_scenes_scenecontainer_set (soyscenesScenecontainer* self, const gchar* key, soyscenesScene* value);
gulong soy_scenes_scenecontainer_get_length (soyscenesScenecontainer* self);
static GObject * soy_scenes_scenecontainer_constructor (GType type, guint n_construct_properties, GObjectConstructParam * construct_properties);
static void soy_scenes_scenecontainer_finalize (GObject* obj);
static void _vala_soy_scenes_scenecontainer_get_property (GObject * object, guint property_id, GValue * value, GParamSpec * pspec);
static void _vala_clear_GMutex (GMutex * mutex);
static void _vala_clear_GRecMutex (GRecMutex * mutex);
static void _vala_clear_GRWLock (GRWLock * mutex);
static void _vala_clear_GCond (GCond * mutex);


soyscenesScenecontainer* soy_scenes_scenecontainer_construct (GType object_type) {
	soyscenesScenecontainer * self = NULL;
	GeeHashMap* _tmp0_ = NULL;
#line 33 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	self = (soyscenesScenecontainer*) g_object_new (object_type, NULL);
#line 34 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	_tmp0_ = gee_hash_map_new (G_TYPE_STRING, (GBoxedCopyFunc) g_strdup, (GDestroyNotify) g_free, SOY_SCENES_TYPE_SCENE, (GBoxedCopyFunc) g_object_ref, (GDestroyNotify) g_object_unref, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
#line 34 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	_g_object_unref0 (self->scenes);
#line 34 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	self->scenes = _tmp0_;
#line 33 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	return self;
#line 102 "Scenecontainer.c"
}


soyscenesScenecontainer* soy_scenes_scenecontainer_new (void) {
#line 33 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	return soy_scenes_scenecontainer_construct (SOY_SCENES_TYPE_SCENECONTAINER);
#line 109 "Scenecontainer.c"
}


soyscenesScene* soy_scenes_scenecontainer_get (soyscenesScenecontainer* self, const gchar* key) {
	soyscenesScene* result = NULL;
	soyscenesScene* res = NULL;
	GeeHashMap* _tmp0_ = NULL;
	GeeSet* _tmp1_ = NULL;
	GeeSet* _tmp2_ = NULL;
	GeeSet* _tmp3_ = NULL;
	const gchar* _tmp4_ = NULL;
	gboolean _tmp5_ = FALSE;
	gboolean _tmp6_ = FALSE;
#line 40 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	g_return_val_if_fail (self != NULL, NULL);
#line 40 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	g_return_val_if_fail (key != NULL, NULL);
#line 41 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	res = NULL;
#line 42 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	g_mutex_lock (&self->mutex);
#line 43 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	_tmp0_ = self->scenes;
#line 43 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	_tmp1_ = gee_abstract_map_get_keys ((GeeAbstractMap*) _tmp0_);
#line 43 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	_tmp2_ = _tmp1_;
#line 43 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	_tmp3_ = _tmp2_;
#line 43 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	_tmp4_ = key;
#line 43 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	_tmp5_ = gee_collection_contains ((GeeCollection*) _tmp3_, _tmp4_);
#line 43 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	_tmp6_ = _tmp5_;
#line 43 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	_g_object_unref0 (_tmp3_);
#line 43 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	if (_tmp6_) {
#line 149 "Scenecontainer.c"
		GeeHashMap* _tmp7_ = NULL;
		const gchar* _tmp8_ = NULL;
		gpointer _tmp9_ = NULL;
#line 44 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
		_tmp7_ = self->scenes;
#line 44 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
		_tmp8_ = key;
#line 44 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
		_tmp9_ = gee_abstract_map_get ((GeeAbstractMap*) _tmp7_, _tmp8_);
#line 44 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
		_g_object_unref0 (res);
#line 44 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
		res = (soyscenesScene*) _tmp9_;
#line 163 "Scenecontainer.c"
	}
#line 45 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	g_mutex_unlock (&self->mutex);
#line 46 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	result = res;
#line 46 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	return result;
#line 171 "Scenecontainer.c"
}


gboolean soy_scenes_scenecontainer_has_key (soyscenesScenecontainer* self, const gchar* key) {
	gboolean result = FALSE;
	gboolean res = FALSE;
	GeeHashMap* _tmp0_ = NULL;
	const gchar* _tmp1_ = NULL;
	gboolean _tmp2_ = FALSE;
#line 49 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	g_return_val_if_fail (self != NULL, FALSE);
#line 49 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	g_return_val_if_fail (key != NULL, FALSE);
#line 51 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	g_mutex_lock (&self->mutex);
#line 52 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	_tmp0_ = self->scenes;
#line 52 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	_tmp1_ = key;
#line 52 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	_tmp2_ = gee_abstract_map_has_key ((GeeAbstractMap*) _tmp0_, _tmp1_);
#line 52 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	res = _tmp2_;
#line 53 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	g_mutex_unlock (&self->mutex);
#line 54 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	result = res;
#line 54 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	return result;
#line 201 "Scenecontainer.c"
}


void soy_scenes_scenecontainer_set (soyscenesScenecontainer* self, const gchar* key, soyscenesScene* value) {
	GeeHashMap* _tmp0_ = NULL;
	const gchar* _tmp1_ = NULL;
	soyscenesScene* _tmp2_ = NULL;
#line 57 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	g_return_if_fail (self != NULL);
#line 57 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	g_return_if_fail (key != NULL);
#line 58 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	g_mutex_lock (&self->mutex);
#line 59 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	g_rw_lock_writer_lock (&soy_scenes__stepLock);
#line 60 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	_tmp0_ = self->scenes;
#line 60 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	_tmp1_ = key;
#line 60 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	_tmp2_ = value;
#line 60 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	gee_abstract_map_set ((GeeAbstractMap*) _tmp0_, _tmp1_, _tmp2_);
#line 61 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	g_rw_lock_writer_unlock (&soy_scenes__stepLock);
#line 62 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	g_mutex_unlock (&self->mutex);
#line 229 "Scenecontainer.c"
}


gulong soy_scenes_scenecontainer_get_length (soyscenesScenecontainer* self) {
	gulong result;
	gulong res = 0UL;
	GeeHashMap* _tmp0_ = NULL;
	gint _tmp1_ = 0;
	gint _tmp2_ = 0;
	gulong _tmp3_ = 0UL;
#line 66 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	g_return_val_if_fail (self != NULL, 0UL);
#line 67 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	res = (gulong) 0;
#line 68 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	g_mutex_lock (&self->mutex);
#line 69 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	_tmp0_ = self->scenes;
#line 69 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	_tmp1_ = gee_abstract_map_get_size ((GeeAbstractMap*) _tmp0_);
#line 69 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	_tmp2_ = _tmp1_;
#line 69 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	res = (gulong) _tmp2_;
#line 70 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	g_mutex_unlock (&self->mutex);
#line 71 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	_tmp3_ = res;
#line 71 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	result = _tmp3_;
#line 71 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	return result;
#line 262 "Scenecontainer.c"
}


static GObject * soy_scenes_scenecontainer_constructor (GType type, guint n_construct_properties, GObjectConstructParam * construct_properties) {
	GObject * obj;
	GObjectClass * parent_class;
	soyscenesScenecontainer * self;
#line 36 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	parent_class = G_OBJECT_CLASS (soy_scenes_scenecontainer_parent_class);
#line 36 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	obj = parent_class->constructor (type, n_construct_properties, construct_properties);
#line 36 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	self = G_TYPE_CHECK_INSTANCE_CAST (obj, SOY_SCENES_TYPE_SCENECONTAINER, soyscenesScenecontainer);
#line 37 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	_vala_clear_GMutex (&self->mutex);
#line 37 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	g_mutex_init (&self->mutex);
#line 36 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	return obj;
#line 282 "Scenecontainer.c"
}


static void soy_scenes_scenecontainer_class_init (soyscenesScenecontainerClass * klass) {
#line 28 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	soy_scenes_scenecontainer_parent_class = g_type_class_peek_parent (klass);
#line 28 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	G_OBJECT_CLASS (klass)->get_property = _vala_soy_scenes_scenecontainer_get_property;
#line 28 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	G_OBJECT_CLASS (klass)->constructor = soy_scenes_scenecontainer_constructor;
#line 28 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	G_OBJECT_CLASS (klass)->finalize = soy_scenes_scenecontainer_finalize;
#line 28 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	g_object_class_install_property (G_OBJECT_CLASS (klass), SOY_SCENES_SCENECONTAINER_LENGTH, g_param_spec_ulong ("length", "length", "length", 0, G_MAXULONG, 0UL, G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB | G_PARAM_READABLE));
#line 297 "Scenecontainer.c"
}


static void soy_scenes_scenecontainer_instance_init (soyscenesScenecontainer * self) {
}


static void soy_scenes_scenecontainer_finalize (GObject* obj) {
	soyscenesScenecontainer * self;
#line 28 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	self = G_TYPE_CHECK_INSTANCE_CAST (obj, SOY_SCENES_TYPE_SCENECONTAINER, soyscenesScenecontainer);
#line 30 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	_g_object_unref0 (self->scenes);
#line 31 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	_vala_clear_GMutex (&self->mutex);
#line 28 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	G_OBJECT_CLASS (soy_scenes_scenecontainer_parent_class)->finalize (obj);
#line 315 "Scenecontainer.c"
}


GType soy_scenes_scenecontainer_get_type (void) {
	static volatile gsize soy_scenes_scenecontainer_type_id__volatile = 0;
	if (g_once_init_enter (&soy_scenes_scenecontainer_type_id__volatile)) {
		static const GTypeInfo g_define_type_info = { sizeof (soyscenesScenecontainerClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) soy_scenes_scenecontainer_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (soyscenesScenecontainer), 0, (GInstanceInitFunc) soy_scenes_scenecontainer_instance_init, NULL };
		GType soy_scenes_scenecontainer_type_id;
		soy_scenes_scenecontainer_type_id = g_type_register_static (G_TYPE_OBJECT, "soyscenesScenecontainer", &g_define_type_info, 0);
		g_once_init_leave (&soy_scenes_scenecontainer_type_id__volatile, soy_scenes_scenecontainer_type_id);
	}
	return soy_scenes_scenecontainer_type_id__volatile;
}


static void _vala_soy_scenes_scenecontainer_get_property (GObject * object, guint property_id, GValue * value, GParamSpec * pspec) {
	soyscenesScenecontainer * self;
	self = G_TYPE_CHECK_INSTANCE_CAST (object, SOY_SCENES_TYPE_SCENECONTAINER, soyscenesScenecontainer);
#line 28 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
	switch (property_id) {
#line 28 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
		case SOY_SCENES_SCENECONTAINER_LENGTH:
#line 28 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
		g_value_set_ulong (value, soy_scenes_scenecontainer_get_length (self));
#line 28 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
		break;
#line 342 "Scenecontainer.c"
		default:
#line 28 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
#line 28 "/home/bmost/libsoy/src/scenes/Scenecontainer.gs"
		break;
#line 348 "Scenecontainer.c"
	}
}


static void _vala_clear_GMutex (GMutex * mutex) {
	GMutex zero_mutex = { 0 };
	if (memcmp (mutex, &zero_mutex, sizeof (GMutex))) {
		g_mutex_clear (mutex);
		memset (mutex, 0, sizeof (GMutex));
	}
}


static void _vala_clear_GRecMutex (GRecMutex * mutex) {
	GRecMutex zero_mutex = { 0 };
	if (memcmp (mutex, &zero_mutex, sizeof (GRecMutex))) {
		g_rec_mutex_clear (mutex);
		memset (mutex, 0, sizeof (GRecMutex));
	}
}


static void _vala_clear_GRWLock (GRWLock * mutex) {
	GRWLock zero_mutex = { 0 };
	if (memcmp (mutex, &zero_mutex, sizeof (GRWLock))) {
		g_rw_lock_clear (mutex);
		memset (mutex, 0, sizeof (GRWLock));
	}
}


static void _vala_clear_GCond (GCond * mutex) {
	GCond zero_mutex = { 0 };
	if (memcmp (mutex, &zero_mutex, sizeof (GCond))) {
		g_cond_clear (mutex);
		memset (mutex, 0, sizeof (GCond));
	}
}



